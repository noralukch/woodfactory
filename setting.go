package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

func (wd *wood) InsertDetailWood(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Grade  string `json:"grade"`
		Detail []struct {
			Width  float64 `json:"width"`
			Length float64 `json:"length"`
			Height float64 `json:"height"`
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	_, err := FindDetailWood(input.Grade)
	if err == nil {
		log.Error("Have grade : ", input.Grade)
		JSON(w, 400, "Have grade : "+input.Grade)
		return
	}
	var tmpGrade Grade
	tmpGrade.Grade = input.Grade

	var Detail []DetailGrade

	for _, size := range input.Detail {
		Size := strconv.FormatFloat(size.Width, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Length, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Height, 'f', -1, 64) + " ม."
		tmpDetail := DetailGrade{
			Size: Size,
		}
		Detail = append(Detail, tmpDetail)
	}

	tmpGrade.DetailGrade = Detail

	err = InsertDetailWood(tmpGrade)
	if err != nil {
		log.Error("Can't insert DetailWood : ", input.Grade)
		JSON(w, 500, "Can't insert DetailWood : "+input.Grade)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
}

func (wd *wood) EditDetailWood(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Grade  string `json:"grade"`
		Detail []struct {
			Width  float64 `json:"width"`
			Length float64 `json:"length"`
			Height float64 `json:"height"`
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	detail, err := FindDetailWood(input.Grade)
	if err != nil {
		log.Error("Can't find : ", input.Grade)
		JSON(w, 404, "Can't find : "+input.Grade)
		return
	}

	check := 0

	for _, size := range input.Detail {
		Size := strconv.FormatFloat(size.Width, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Length, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Height, 'f', -1, 64) + " ม."
		for _, val := range detail.DetailGrade {
			if Size == val.Size {
				check = 1
			}
		}
		if check == 0 {
			tmpDetail := DetailGrade{
				Size: Size,
			}
			detail.DetailGrade = append(detail.DetailGrade, tmpDetail)
		}
		check = 0
	}

	err = UpdateDetailWood(detail)
	if err != nil {
		log.Error("Can't update DetailWood : ", input.Grade)
		JSON(w, 500, "Can't update DetailWood : "+input.Grade)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
}

func (wd *wood) DeleteGrade(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Grade string `json:"grade"`
	}

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	_, err := FindDetailWood(input.Grade)
	if err != nil {
		log.Error("Can't find : ", input.Grade)
		JSON(w, 404, "Can't find : "+input.Grade)
		return
	}

	err = DelDetailWood(input.Grade)
	if err != nil {
		log.Error("Can't delete : ", input.Grade)
		JSON(w, 500, "Can't delete : "+input.Grade)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
}

func (wd *wood) DeleteSize(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Grade  string `json:"grade"`
		Detail []struct {
			Width  float64 `json:"width"`
			Length float64 `json:"length"`
			Height float64 `json:"height"`
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	detail, err := FindDetailWood(input.Grade)
	if err != nil {
		log.Error("Can't find : ", input.Grade)
		JSON(w, 404, "Can't find : "+input.Grade)
		return
	}

	for _, size := range input.Detail {
		Size := strconv.FormatFloat(size.Width, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Length, 'f', -1, 64) + "\"x " + strconv.FormatFloat(size.Height, 'f', -1, 64) + " ม."
		for i, val := range detail.DetailGrade {
			if Size == val.Size {
				first := detail.DetailGrade[:i]
				second := detail.DetailGrade[i+1:]
				detail.DetailGrade = first
				detail.DetailGrade = append(detail.DetailGrade, second...)
			}
		}

	}

	err = UpdateDetailWood(detail)
	if err != nil {
		log.Error("Can't update DetailWood : ", input.Grade)
		JSON(w, 500, "Can't update DetailWood : "+input.Grade)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
}

func (wd *wood) GetDetailWood(w http.ResponseWriter, r *http.Request) {

	g, err := FindAllDetailWood()
	if err != nil {
		log.Error("Can't find all detailwood")
		JSON(w, 404, "Can't find all detailwood")
		return
	}

	res := struct {
		Grade []Grade `json:"grade"`
	}{g}

	JSON(w, 200, res)
}
