package main

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

var testW *wood

func Test_wood_GetEmployee(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/getemployee", nil)

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Test",
			testW,
			args{w, req},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetEmployee(tt.args.w, tt.args.r)
			//log.Println(w.Body.String())
		})
	}
}

func Test_wood_InsertEmployee(t *testing.T) {
	data := []struct {
		Name       string
		Surname    string
		Nickname   string
		Gender     string
		Age        int
		National   string
		Tel        string
		Address    string
		Department string
		Position   string
		SalaryType string
		Email      string
	}{
		{
			"",
			"C",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"A",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"D",
			"C",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"QC",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"นิตยา",
			"มีชอบ",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"A",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"B",
			"A",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"AB",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"X",
			"aaa",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"QC",
			"หัวหน้าแผนก",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
	}

	var req *http.Request
	Employee := url.Values{}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
	}{
		// TODO: Add test cases.
		{
			"Not have info",
			testW,
		},
		{
			"Insert Employee",
			testW,
		},
		{
			"Name & Surname was use",
			testW,
		},
		{
			"Insert Employee but don't have department in system",
			testW,
		},
		{
			"In department had manager",
			testW,
		},
	}

	for i, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			z, _ := os.Open("C:/Users/monic/OneDrive/รูปภาพ/11.png")
			a, _ := z.Stat()

			defer z.Close()

			body := &bytes.Buffer{}
			writer := multipart.NewWriter(body)
			part, _ := writer.CreateFormFile("PictureEmployee", a.Name())
			io.Copy(part, z)
			writer.Close()

			Em, _ := json.Marshal(data[i])
			Employee.Set("Employee", string(Em))
			req = httptest.NewRequest("POST", "/insertemployee", body)
			req.PostForm = Employee
			req.Header.Add("Content-Type", writer.FormDataContentType())
			tt.wd.InsertEmployee(w, req)
		})
	}
}

func Test_wood_UpdateEmployee(t *testing.T) {
	data := []struct {
		EmployeeID string
		NameTitle  string
		Name       string
		Surname    string
		Nickname   string
		Gender     string
		Age        int
		National   string
		Tel        string
		Address    string
		Department string
		Position   string
		SalaryType string
		Email      string
	}{
		{
			"",
			"C",
			"C",
			"C",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"A",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"999",
			"C",
			"X",
			"X",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"A",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"180005",
			"นาง",
			"นิตยา",
			"มีชอบ",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"A",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"180005",
			"นาง",
			"B",
			"A",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"AB",
			"A",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"180007",
			"นาง",
			"X",
			"A",
			"A",
			"หญิง",
			20,
			"A",
			"A",
			"A",
			"QC",
			"หัวหน้าแผนก",
			"รายเดือน",
			"kv_woodfac@hotmail.com",
		},
		{
			"180022",
			"นาย",
			"วุฒิชาติซัง",
			"สวัสดีครับ",
			" สวัสดีค่ะ",
			"ชาย",
			99,
			"ไทย",
			"0912345687",
			"TNI",
			"QC",
			"หัวหน้าพี่บอส",
			"รายเดือน",
			"test@hotmail.com",
		},
	}

	var req *http.Request
	Employee := url.Values{}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
	}{
		// TODO: Add test cases.
		{
			"Not have info",
			testW,
		},
		{
			"No Employee",
			testW,
		},
		{
			"Name & Surname was use",
			testW,
		},
		{
			"Insert Employee but don't have department in system",
			testW,
		},
		{
			"Update manager to other department",
			testW,
		},
		{
			"Update employee success",
			testW,
		},
	}

	for i, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			z, _ := os.Open("C:/Users/monic/OneDrive/รูปภาพ/11.png")
			a, _ := z.Stat()

			defer z.Close()

			body := &bytes.Buffer{}
			writer := multipart.NewWriter(body)
			part, _ := writer.CreateFormFile("PictureEmployee", a.Name())
			io.Copy(part, z)
			writer.Close()

			Em, _ := json.Marshal(data[i])
			Employee.Set("Employee", string(Em))
			req = httptest.NewRequest("PUT", "/updateemployee", body)
			req.PostForm = Employee
			req.Header.Add("Content-Type", writer.FormDataContentType())
			tt.wd.UpdateEmployee(w, req)
		})
	}
}

func Test_wood_DeleteEmployee(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()

	EmployeeID := []string{"", "8989", "4"}

	var req []*http.Request

	for i, _ := range EmployeeID {
		r := httptest.NewRequest("DELETE", "/deleteemployee", nil)
		r.Header.Set("EmployeeID", EmployeeID[i])
		req = append(req, r)
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"No have info",
			testW,
			args{w, req[0]},
		},
		{
			"No Employee",
			testW,
			args{w, req[1]},
		},
		// {
		// 	"Delete Success",
		// 	testW,
		// 	args{w, req[2]},
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.DeleteEmployee(tt.args.w, tt.args.r)
		})
	}
}
