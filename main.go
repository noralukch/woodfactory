package main

import (
	"log"
	"net/http"
	"time"

	"github.com/joho/godotenv"

	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

func main() {

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowCredentials: true,
	})

	err := godotenv.Load("/opt/.env")
	if err != nil {
		panic(err)
	}

	router := newRouter()
	n := negroni.New()
	n.Use(negroni.NewRecovery())
	n.Use(logrusMiddleware())
	n.Use(c)
	n.UseHandler(router)
	n.Run(":5000")

}

func logrusMiddleware() negroni.HandlerFunc {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		start := time.Now()
		next(w, r)

		res := w.(negroni.ResponseWriter)
		log.Println("Completed ",
			r.Method,
			r.URL.Path,
			res.Status(),
			http.StatusText(res.Status()),
			time.Since(start))
	})
}
