package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"

	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/sha3"
	gomail "gopkg.in/gomail.v2"
	"gopkg.in/mgo.v2/bson"
)

type wood struct{}

func (wd *wood) hello(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintln(w, "Hellooooo")
}

func (wd *wood) Option(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	return
}

func (wd *wood) RegisterUser(w http.ResponseWriter, r *http.Request) {
	var user struct {
		EmployeeID string `json:"employeeid" `
		Password   string `json:"password" `
		Role       string `json:"role" `
	}

	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	if _, err := FindUser(user.EmployeeID); err == nil {
		log.Errorln("This ID was use", err)
		JSON(w, 400, "This ID was use")
		return
	}

	_, err := FindEmployee(user.EmployeeID)
	if err != nil {
		log.Errorln("No Employee ID", err)
		JSON(w, 204, "No Employee ID")
		return
	}

	salt := GenerateSalt()
	hash := base64.StdEncoding.EncodeToString(CalculateHash(user.EmployeeID, user.Password, salt))

	tmpUser := User{
		EmployeeID: user.EmployeeID,
		Salt:       base64.StdEncoding.EncodeToString(salt),
		Hash:       hash,
		Role:       user.Role,
	}

	err = InsertUser(tmpUser)
	if err != nil {
		log.Errorln("Can't login", err)
		JSON(w, 500, "Can't login")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) LoginUser(w http.ResponseWriter, r *http.Request) {
	var user struct {
		EmployeeID string `json:"employeeid" `
		Password   string `json:"password" `
	}

	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	us, err := FindUser(user.EmployeeID)
	if err != nil {
		log.Errorln("Can't get Login : ", user.EmployeeID)
		JSON(w, 204, "Can't get Login : "+user.EmployeeID)
		return
	}

	byteSalt, _ := base64.StdEncoding.DecodeString(us.Salt)
	hash := base64.StdEncoding.EncodeToString(CalculateHash(user.EmployeeID, user.Password, byteSalt))

	if hash != us.Hash {
		log.Errorln("Password is Wrong")
		JSON(w, http.StatusBadRequest, "Password is Wrong")
		return
	}

	token := base64.StdEncoding.EncodeToString(CalculateToken(user.EmployeeID, us.Role))

	res := struct {
		Token string `json:"token" `
		Role  string `json:"role" `
	}{token, us.Role}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) GetUser(w http.ResponseWriter, r *http.Request) {

	us, err := FindAllUser()
	if err != nil {
		log.Errorln("Can't Get All User ", err)
		JSON(w, 500, "Can't Get All User ")
		return
	}

	var user []struct {
		ID         bson.ObjectId `json:"_id"`
		EmployeeID string        `json:"employeeid"`
		Name       string        `json:"name"`
		Department string        `json:"department"`
		Role       string        `json:"role"`
	}

	for _, value_user := range us {
		if value_user.EmployeeID != "admin" {
			em, err := FindEmployee(value_user.EmployeeID)
			if err != nil {
				log.Errorln("Can't fine this employee : ", em.EmployeeID)
				JSON(w, 204, "Can't fine this employee : "+em.EmployeeID)
				return
			}
			tmpUser := struct {
				ID         bson.ObjectId `json:"_id"`
				EmployeeID string        `json:"employeeid"`
				Name       string        `json:"name"`
				Department string        `json:"department"`
				Role       string        `json:"role"`
			}{
				value_user.ID, value_user.EmployeeID, em.NameTitle + " " + em.Name + " " + em.Surname, em.Department, value_user.Role,
			}

			user = append(user, tmpUser)
		} else {
			tmpUser := struct {
				ID         bson.ObjectId `json:"_id"`
				EmployeeID string        `json:"employeeid"`
				Name       string        `json:"name"`
				Department string        `json:"department"`
				Role       string        `json:"role"`
			}{
				value_user.ID, value_user.EmployeeID, "", "", value_user.Role,
			}

			user = append(user, tmpUser)
		}
	}

	res := struct {
		User []struct {
			ID         bson.ObjectId `json:"_id"`
			EmployeeID string        `json:"employeeid"`
			Name       string        `json:"name"`
			Department string        `json:"department"`
			Role       string        `json:"role"`
		}
	}{user}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) EditUserRole(w http.ResponseWriter, r *http.Request) {
	var input struct {
		EmployeeID string `json:"employeeid"`
		Role       string `json:"role"`
	}

	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	us, err := FindUser(input.EmployeeID)
	if err != nil {
		log.Errorln("Can't find EmployeeID : ", input.EmployeeID)
		JSON(w, 204, "Can't find EmployeeID : "+input.EmployeeID)
		return
	}

	us.Role = input.Role

	err = UpdateUser(us)
	if err != nil {
		log.Errorln("Can't Update User : ", err)
		JSON(w, http.StatusInternalServerError, "Can't Update User")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) GetUserDetail(w http.ResponseWriter, r *http.Request) {
	ID, Role, err := CheckToken(w, r)
	if err != nil {
		log.Error("Token Wrong")
		JSON(w, 400, "Token Wrong")
		return
	}

	defer r.Body.Close()

	if ID != "admin" {
		em, err := FindEmployee(ID)
		if err != nil {
			log.Errorln("Can't fine this employee : ", em.EmployeeID)
			JSON(w, 204, "Can't fine this employee : "+em.EmployeeID)
			return
		}
		res := struct {
			Employee Employee `json:"employee" `
			Role     string   `json:"role" `
		}{em, Role}

		JSON(w, http.StatusOK, res)
	} else {
		res := struct {
			ID   string `json:"id" `
			Role string `json:"role" `
		}{ID, Role}
		JSON(w, http.StatusOK, res)
	}
	return
}

func (wd *wood) SendNewpasswordToEmail(w http.ResponseWriter, r *http.Request) {
	var user struct {
		EmployeeID string `json:"employeeid" `
	}

	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	us, err := FindUser(user.EmployeeID)
	if err != nil {
		log.Errorln("Can't find EmployeeID : ", user.EmployeeID)
		JSON(w, 204, "Can't find EmployeeID : "+user.EmployeeID)
		return
	}

	token, err := GenerateRandomBytes(32)
	if err != nil {
		log.Println("Can't generate bytes")
		JSON(w, 500, "Can't generate bytes")
		return
	}
	NewPW := binary.BigEndian.Uint64(token)
	str_newpw := strconv.FormatUint(NewPW, 10)
	str_newpw = str_newpw[:8]
	salt := GenerateSalt()
	hash := base64.StdEncoding.EncodeToString(CalculateHash(user.EmployeeID, str_newpw, salt))

	us.Salt = base64.StdEncoding.EncodeToString(salt)
	us.Hash = hash

	err = UpdateUser(us)
	if err != nil {
		log.Errorln("Can't Update User : ", err)
		JSON(w, http.StatusInternalServerError, "Can't Update User")
		return
	}

	Sender := os.Getenv("Sender")
	Receiver := os.Getenv("Receiver")

	m := gomail.NewMessage()
	m.SetHeader("From", Sender)
	m.SetHeader("To", Receiver)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "Change Password")
	m.SetBody("text/html", fmt.Sprint("EmployeeID : "+us.EmployeeID+"\nNew Password : "+str_newpw))
	//m.Attach("/home/Alex/lolcat.jpg")

	//d := gomail.NewDialer("smtp.gmail.com", 587, "noraluk.kn@gmail.com", "kanoon2499")
	d := gomail.NewPlainDialer("smtp.gmail.com", 587, Sender, "crafity123")
	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) ChangePassword(w http.ResponseWriter, r *http.Request) {

	var user struct {
		EmployeeID string `json:employeeid`
		Password   string `json:"password" `
	}

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	us, err := FindUser(user.EmployeeID)
	if err != nil {
		log.Errorln("Can't find EmployeeID : ", user.EmployeeID)
		JSON(w, 204, "Can't find Employeeuser.EmployeeID : "+user.EmployeeID)
		return
	}

	salt := GenerateSalt()
	hash := base64.StdEncoding.EncodeToString(CalculateHash(user.EmployeeID, user.Password, salt))

	us.Salt = base64.StdEncoding.EncodeToString(salt)
	us.Hash = hash

	err = UpdateUser(us)
	if err != nil {
		log.Errorln("Can't Update User : ", err)
		JSON(w, http.StatusInternalServerError, "Can't Update User")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) DeleteUser(w http.ResponseWriter, r *http.Request) {
	User := r.Header.Get("User")

	defer r.Body.Close()

	us, err := FindUser(User)
	if err != nil {
		log.Println("Can't find User : ", User)
		JSON(w, 204, "Can't find User : "+User)
		return
	}

	err = DelUser(us.EmployeeID)
	if err != nil {
		log.Println("Can't Delete User : ", err)
		JSON(w, 500, "Can't Delete User")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func GenerateSalt() []byte {
	salt := make([]byte, 32)
	_, _ = io.ReadFull(rand.Reader, salt)
	return salt
}

func CalculateHash(username string, password string, salt []byte) []byte {
	data := append([]byte(username), []byte(password)...)
	data = append(data, salt...)
	hash := sha3.Sum512(data)
	return hash[:]
}

func CalculateToken(id string, role string) []byte {
	t := time.Now().Format("20060102150405")
	log.Println("time:", t)
	sign := "|"
	data := append([]byte(id), []byte(sign)...)
	data = append(data, []byte(role)...)
	data = append(data, []byte(sign)...)
	data = append(data, []byte(t)...)
	return data[:]
}

func JSON(w http.ResponseWriter, status int, v interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)

	if err := json.NewEncoder(w).Encode(v); err != nil {
		log.Errorln("JSON Response Encode error:", err)
		return
	}
}

func CheckToken(w http.ResponseWriter, r *http.Request) (string, string, error) {
	token := r.Header.Get("Authorization")
	if token == "" {
		return "", "", errors.New("not found")
	}
	// log.Println(token)
	DecodeToken, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		log.Errorln("Can't Decode Token")
		JSON(w, http.StatusBadRequest, "Can't Decode Token")
		return "", "", err
	}
	StrToken := string(DecodeToken[:])
	// log.Println("ID:", StrToken)
	num := strings.Index(StrToken, "|")
	id := StrToken[:num]
	// log.Println("ID:", id)
	StrToken = StrToken[num+1:]
	// log.Println("StrToken:", StrToken)
	num2 := strings.Index(StrToken, "|")
	Role := StrToken[:num2]

	// log.Println("Role:", Role)
	// Int_TimeNow, _ := strconv.Atoi(t)
	// Int_TimeToken, _ := strconv.Atoi(TokenTime)

	// log.Println(Int_TimeNow)
	// log.Println(Int_TimeToken)

	// log.Println(Int_TimeNow - Int_TimeToken)
	// if Int_TimeNow-Int_TimeToken > 80 {
	// 	return "Long", err
	// }
	return id, Role, nil
}

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}
