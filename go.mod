module WoodFactory

require (
	github.com/Luxurioust/excelize v1.4.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/pprof v0.0.0-20181206194817-3ea8567a2e57 // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.6.2
	github.com/hgfischer/go-otp v1.0.0
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/ibm-security-innovation/libsecurity-go v0.0.0-20160221131408-52112201f91b
	github.com/joho/godotenv v1.3.0
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rs/cors v1.6.0
	github.com/sirupsen/logrus v1.0.6
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/subosito/gotenv v1.1.1
	github.com/tealeg/xlsx v1.0.3
	github.com/urfave/negroni v0.3.0
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
	golang.org/x/crypto v0.0.0-20180808211826-de0752318171
	golang.org/x/sys v0.0.0-20180816055513-1c9583448a9c
	golang.org/x/tools v0.0.0-20181207222222-4c874b978acb // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc
	gopkg.in/gomail.v2 v2.0.0-20150902115704-41f357289737
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
