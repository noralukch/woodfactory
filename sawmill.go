package main

import (
	"encoding/json"
	"math"

	"net/http"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

func (wd *wood) InsertTable(w http.ResponseWriter, r *http.Request) {
	var table []struct {
		Employeeid string  `json:"employeeid"`
		Position   string  `json:"position"`
		Rate       float64 `json:"rate"`
	}

	if err := json.NewDecoder(r.Body).Decode(&table); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	sum := 0.0

	for i, a := range table {
		for j, b := range table {
			if i != j {
				if a.Employeeid == b.Employeeid {
					log.Errorln("Employee is repeat : ", a.Employeeid)
					JSON(w, 400, "Employee is repeat : "+a.Employeeid)
					return
				}
				if a.Position == "นายม้า" && a.Position == b.Position {
					log.Errorln("นายม้า is repeat")
					JSON(w, 400, "นายม้า is repeat")
					return
				} else if a.Position == "หางม้า" && a.Position == b.Position {
					log.Errorln("หางม้า is repeat")
					JSON(w, 400, "หางม้า is repeat")
					return
				}
			}
		}
		if strings.Compare(a.Position, "นายม้า") != 0 && strings.Compare(a.Position, "หางม้า") != 0 {
			log.Errorln("Position is not correct")
			JSON(w, 400, "Position is not correct")
			return
		}
		sum = sum + a.Rate
	}

	if sum > 1 || sum < 1 {
		log.Errorln("Rate is not equal 100 %")
		JSON(w, 400, "Rate is not equal 100 %")
		return
	}

	date := time.Now().Format("02/01/2006")

	_, err := FindDateSawmill(date)
	if err != nil {
		tmpSawmill := Sawmill{
			Date: date,
		}
		err := InsertSawmill(tmpSawmill)
		if err != nil {
			log.Errorln("Can't Insert Date ", err)
			JSON(w, 500, "Can't Insert Date")
			return
		}
		tmpTotal := Total{
			Date: date,
		}
		err = InsertTotalSawmill(tmpTotal)
		if err != nil {
			log.Errorln("Can't Insert Date ", err)
			JSON(w, 500, "Can't Insert Date")
			return
		}
	}

	sm, err := FindDateSawmill(date)
	if err != nil {
		log.Errorln("Can't find Sawmill today ", err)
		JSON(w, 204, "Can't find Sawmill today")
		return
	}

	total, err := FindDateTotal(date)
	if err != nil {
		log.Errorln("Can't find Total Sawmill today ", err)
		JSON(w, 204, "Can't find Sawmill today")
		return
	}

	for _, no := range sm.NoTable {
		for _, tb := range no.EmTable {
			for _, tmpem := range table {
				if tmpem.Employeeid == tb.Employeeid {
					log.Errorln("Employee is in other table : ", tmpem.Employeeid)
					JSON(w, 400, "Employee is in other table : "+tmpem.Employeeid)
					return
				}
			}
		}
	}

	var ArrayEmtable []EmTable

	for _, a := range table {
		em, err := FindEmployee(a.Employeeid)
		if err != nil {
			log.Errorln("Cant find this employee : ", a.Employeeid)
			JSON(w, 204, "Cant find this employee : "+a.Employeeid)
			return
		}
		if em.Department != "รายวันโต๊ะเลื่อย" {
			log.Errorln("This employee haven't in รายวันโต๊ะเลื่อย")
			JSON(w, 400, "This employee haven't in รายวันโต๊ะเลื่อย")
			return
		}
		tmpEmTable := EmTable{
			Employeeid: a.Employeeid,
			NameTitle:  em.NameTitle,
			Employee:   em.Name + " " + em.Surname,
			Position:   a.Position,
			Rate:       a.Rate,
		}
		ArrayEmtable = append(ArrayEmtable, tmpEmTable)
	}

	NoTable := len(sm.NoTable)

	if NoTable == 0 {
		NoTable = NoTable + 1
	} else {
		len := 1
		for _, value_no := range sm.NoTable {
			no, _ := strconv.Atoi(value_no.No)
			if len != no {
				NoTable = len
				break
			} else if NoTable == no {
				NoTable = NoTable + 1
			}
			len++
		}
	}

	grade, err := FindAllDetailWood()
	if err != nil {
		log.Error("Can't find all detailwood ", err)
		JSON(w, 500, "Can't find all detailwood")
		return
	}

	tmpTable := Notable{
		No:      strconv.Itoa(NoTable),
		Grade:   grade,
		EmTable: ArrayEmtable,
		Status:  "",
	}

	var tmp []Notable
	len := 1
	check := 0
	if NoTable == 1 {
		tmp = append(tmp, tmpTable)
		tmp = append(tmp, sm.NoTable...)
		sm.NoTable = tmp
		check = 1
	} else {
		for i, value_no := range sm.NoTable {
			no, _ := strconv.Atoi(value_no.No)

			if len != no {
				tmp = append(tmp, sm.NoTable[:i]...)
				tmp = append(tmp, tmpTable)
				tmp = append(tmp, sm.NoTable[i:]...)
				sm.NoTable = tmp
				check = 1
				break
			}
			len++
		}
	}
	if check == 0 {
		sm.NoTable = append(sm.NoTable, tmpTable)
	}

	err = UpdateSawmill(sm)
	if err != nil {
		log.Errorln("Can't Update Table : ", sm.Date)
		JSON(w, 500, "Can't Update Table :"+sm.Date)
		return
	}

	var tmpSumGrade []SumGrade

	for _, val := range grade {
		tmp := SumGrade{
			Grade: val.Grade,
		}
		tmpSumGrade = append(tmpSumGrade, tmp)
	}

	tmpSumTable := SumTable{
		No:       strconv.Itoa(NoTable),
		SumGrade: tmpSumGrade,
	}

	var tmp2 []SumTable
	len = 1
	check = 0
	if NoTable == 1 {
		tmp2 = append(tmp2, tmpSumTable)
		tmp2 = append(tmp2, total.SumTable...)
		total.SumTable = tmp2
		check = 1
	} else {
		for i, value_no := range total.SumTable {
			no, _ := strconv.Atoi(value_no.No)
			if len != no {
				tmp2 = append(tmp2, total.SumTable[:i]...)
				tmp2 = append(tmp2, tmpSumTable)
				tmp2 = append(tmp2, total.SumTable[i:]...)
				total.SumTable = tmp2
				check = 1
				break
			}
			len++
		}
	}

	if check == 0 {
		total.SumTable = append(total.SumTable, tmpSumTable)
	}

	err = UpdateTotalSawmill(total)
	if err != nil {
		log.Errorln("Can't Update Summarize Sawmill : ", total.Date)
		JSON(w, 500, "Can't Update Summarize Sawmill :"+total.Date)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) UpdateEmployeeInTable(w http.ResponseWriter, r *http.Request) {
	var table struct {
		Date    string `json:"date"`
		Notable string `json:"notable"`
		Emtable []struct {
			Employeeid string  `json:"employeeid"`
			Position   string  `json:"position"`
			Rate       float64 `json:"rate"`
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&table); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusBadRequest, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	sum := 0.0

	for i, a := range table.Emtable {
		for j, b := range table.Emtable {
			if i != j {
				if a.Employeeid == b.Employeeid {
					log.Errorln("Employee is repeat : ", a.Employeeid)
					JSON(w, 400, "Employee is repeat : "+a.Employeeid)
					return
				}
				if a.Position == "นายม้า" && a.Position == b.Position {
					log.Errorln("นายม้า is repeat  ")
					JSON(w, 400, "นายม้า is repeat ")
					return
				} else if a.Position == "หางม้า" && a.Position == b.Position {
					log.Errorln("หางม้า is repeat  ")
					JSON(w, 400, "หางม้า is repeat ")
					return
				}
			}
		}
		if strings.Compare(a.Position, "นายม้า") != 0 && strings.Compare(a.Position, "หางม้า") != 0 {
			log.Errorln("Position is not correct")
			JSON(w, 400, "Position is not correct")
			return
		}
		sum = sum + a.Rate
	}

	if sum > 1 || sum < 1 {
		log.Errorln("Rate is not equal 100 % ")
		JSON(w, 400, "Rate is not equal 100 % ")
		return
	}

	sm, err := FindDateSawmill(table.Date)
	if err != nil {
		log.Errorln("Can't find this date : ", table.Date)
		JSON(w, 204, "Can't find this date : "+table.Date)
		return
	}

	for _, no := range sm.NoTable {
		if no.No != table.Notable {
			for _, tb := range no.EmTable {
				for _, tmpem := range table.Emtable {
					if tmpem.Employeeid == tb.Employeeid {
						log.Errorln("Employee is in other table : ", tmpem.Employeeid)
						JSON(w, 400, "Employee is in other table : "+tmpem.Employeeid)
						return
					}
				}
			}
		}
	}

	sort := 1
	check := 0

	for i, no := range sm.NoTable {
		if table.Notable == no.No {
			check = 1
			for _, value := range table.Emtable {
				em, err := FindEmployee(value.Employeeid)
				if err != nil {
					log.Errorln("Cant find this employee : ", value.Employeeid)
					JSON(w, 204, "Cant find this employee : "+value.Employeeid)
					return
				}
				if em.Department != "รายวันโต๊ะเลื่อย" {
					log.Errorln("This employee haven't in รายวันโต๊ะเลื่อย ")
					JSON(w, 400, "This employee haven't in รายวันโต๊ะเลื่อย ")
					return
				}
				if value.Position == "นายม้า" {
					sm.NoTable[i].EmTable[0].Employeeid = em.EmployeeID
					sm.NoTable[i].EmTable[0].NameTitle = em.NameTitle
					sm.NoTable[i].EmTable[0].Employee = em.Name + " " + em.Surname
					sm.NoTable[i].EmTable[0].Position = value.Position
					sm.NoTable[i].EmTable[0].Rate = value.Rate
				} else if value.Position == "หางม้า" {
					sm.NoTable[i].EmTable[sort].Employeeid = em.EmployeeID
					sm.NoTable[i].EmTable[sort].NameTitle = em.NameTitle
					sm.NoTable[i].EmTable[sort].Employee = em.Name + " " + em.Surname
					sm.NoTable[i].EmTable[sort].Position = value.Position
					sm.NoTable[i].EmTable[sort].Rate = value.Rate
					sort++
				}
			}
			break
		}
	}

	if check == 0 {
		log.Errorln("Table don't have  : ", table.Notable)
		JSON(w, 204, "Table don't have  : "+table.Notable)
		return
	}
	err = UpdateSawmill(sm)
	if err != nil {
		log.Errorln("Can't update this employee in table : ", table.Notable)
		JSON(w, 500, "Can't update this employee in table : "+table.Notable)
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, 200, res)
	return
}

var list []string

func (wd *wood) CheckDate(w http.ResponseWriter, r *http.Request) {
	var wood struct {
		Date       string `json:"date"`
		EmployeeID string `json:"employeeid"`
	}

	if err := json.NewDecoder(r.Body).Decode(&wood); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	date2, err := time.Parse("02/01/2006", wood.Date)
	if err != nil {
		log.Errorln("Can't Parse time")
		JSON(w, 500, "Can't Parse time")
		return
	}

	_, err = FindUser(wood.EmployeeID)
	if err != nil {
		log.Errorln("Cant find this employee : ", wood.EmployeeID)
		JSON(w, 204, "Cant find this employee : "+wood.EmployeeID)
		return
	}

	duration := time.Since(date2)

	if math.Floor(duration.Hours()/24) > 1 {
		auth := AuthEmployee{
			EmployeeID: wood.EmployeeID,
			Date:       wood.Date,
			Status:     false,
		}
		err := InsertAuthEmployee(auth)
		if err != nil {
			JSON(w, 500, "Can't insert auth")
			return
		}
		res := struct {
			Success bool `json:"success"`
		}{false}
		JSON(w, 401, res)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}
	JSON(w, 200, res)
	return
}

func (wd *wood) ListAuthorize(w http.ResponseWriter, r *http.Request) {
	auth, err := FindAllAuthEmployee()
	if err != nil {
		JSON(w, 500, "Auth not found")
		return
	}

	JSON(w, 200, auth)
	return
}

func (wd *wood) Validate(w http.ResponseWriter, r *http.Request) {
	var data struct {
		EmployeeID string `json:"employeeid"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	auth, err := FindAuthEmployee(data.EmployeeID)
	if err != nil {
		log.Errorln("Cant find this employee : ", data.EmployeeID)
		JSON(w, 204, "Cant find this employee : "+data.EmployeeID)
		return
	}

	auth.Status = true

	err = UpdateAuthEmployee(auth)
	if err != nil {
		JSON(w, 500, "Can't update auth")
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}
	JSON(w, 200, res)
	return

}

func (wd *wood) RemoveAuth(w http.ResponseWriter, r *http.Request) {
	var data struct {
		EmployeeID string `json:"employeeid"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	auth, err := FindAuthEmployee(data.EmployeeID)
	if err != nil {
		log.Errorln("Cant find this employee : ", data.EmployeeID)
		JSON(w, 204, "Cant find this employee : "+data.EmployeeID)
		return
	}

	err = DeleteAuthEmployee(auth.EmployeeID)
	if err != nil {
		JSON(w, 500, "Can't delete auth")
		return
	}
	res := struct {
		Success bool `json:"success"`
	}{true}
	JSON(w, 200, res)
	return
}

func (wd *wood) UpdateDetailWood(w http.ResponseWriter, r *http.Request) {
	var wood struct {
		Date    string `json:"date"`
		Notable string `json:"notable"`
		Grade   string `json:"grade"`
		Detail  []struct {
			Size          string  `json:"size"`
			AmountWood    float64 `json:"amountwood"`
			AmountBadwood float64 `json:"amountbadwood"`
			Cost          float64 `json:"cost"`
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&wood); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	sm_date, err := FindDateSawmill(wood.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", wood.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+wood.Date)
		return
	}

	// sm_total, err := FindDateTotal(wood.Date)
	// if err != nil {
	// 	log.Errorln("Can't find date total :", wood.Date)
	// 	JSON(w, 204, "Can't find date total :"+wood.Date)
	// 	return
	// }

	check := 0
	checkgrade := 0

	for _, value1 := range sm_date.NoTable {
		if strings.Compare(value1.No, wood.Notable) == 0 {
			check = 1
			for _, value2 := range value1.Grade {
				if strings.Compare(value2.Grade, wood.Grade) == 0 {
					checkgrade = 1
					for _, value3 := range value2.DetailGrade {
						for i, data := range wood.Detail {
							if strings.Compare(data.Size, value3.Size) == 0 {
								break
							} else if strings.Compare(data.Size, value3.Size) == 0 && len(wood.Detail)-1 == i {
								log.Println("Size isn't correct")
								JSON(w, 400, "Size isn't correct")
								return
							}
						}
					}
				}
			}
		}
	}

	if check == 0 {
		log.Errorln("Table don't have  : ", wood.Notable)
		JSON(w, 204, "Table don't have  : "+wood.Notable)
		return
	}

	if checkgrade == 0 {
		log.Errorln("Grade don't have  : ", wood.Grade)
		JSON(w, 204, "Grade don't have  : "+wood.Grade)
		return
	}

	for i, no := range sm_date.NoTable {
		if no.No == wood.Notable {
			for j, grade := range no.Grade {
				if grade.Grade == wood.Grade {
					for k, detail := range grade.DetailGrade {
						for _, wood := range wood.Detail {
							if wood.Size == detail.Size {
								num_width := strings.Index(wood.Size, "\"")
								width, _ := strconv.ParseFloat(wood.Size[:num_width], 64)
								wood.Size = wood.Size[num_width+3:]
								num_length := strings.Index(wood.Size, "\"")
								length, _ := strconv.ParseFloat(wood.Size[:num_length], 64)
								wood.Size = wood.Size[num_length+3:]
								num_height := strings.Index(wood.Size, "ม")
								height, _ := strconv.ParseFloat(wood.Size[:num_height-1], 64)

								foot_Wood := width * length * height * wood.AmountWood * 0.0228
								foot_Badwood := width * length * height * wood.AmountBadwood * 0.0228
								Remain := wood.AmountWood - wood.AmountBadwood
								foot_Remain := width * length * height * Remain * 0.0228
								Wage := foot_Remain * wood.Cost

								sm_date.NoTable[i].Grade[j].DetailGrade[k].AmountWood = wood.AmountWood
								sm_date.NoTable[i].Grade[j].DetailGrade[k].FootWood = foot_Wood
								sm_date.NoTable[i].Grade[j].DetailGrade[k].AmountBadwood = wood.AmountBadwood
								sm_date.NoTable[i].Grade[j].DetailGrade[k].FootBadwood = foot_Badwood
								sm_date.NoTable[i].Grade[j].DetailGrade[k].Remain = Remain
								sm_date.NoTable[i].Grade[j].DetailGrade[k].FootRemain = foot_Remain
								sm_date.NoTable[i].Grade[j].DetailGrade[k].Wage = Wage
								sm_date.NoTable[i].Grade[j].DetailGrade[k].CostWood = wood.Cost
							}
						}
					}
				}
			}
			sm_date.NoTable[i].Status = "true"
		}
	}

	err = UpdateSawmill(sm_date)
	if err != nil {
		log.Errorln("Can't update sawmill this date : ", wood.Date)
		JSON(w, http.StatusInternalServerError, "Can't update sawmill this date : "+wood.Date)
		return
	}

	err = SumWood(wood.Date, wood.Notable, wood.Grade)
	if err != nil {
		log.Errorln("Process of SumWood is error ", err)
		JSON(w, 500, "Process of SumWood is error")
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) UpdateWeightWood(w http.ResponseWriter, r *http.Request) {
	var wood struct {
		Date       string  `json:"date"`
		Notable    string  `json:"notable"`
		WeightWood float64 `json:"weightwood"`
		WeightWing float64 `json:"weightwing"`
		Remark     string  `json:"remark"`
	}

	if err := json.NewDecoder(r.Body).Decode(&wood); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	if wood.WeightWood <= 0 || wood.WeightWing <= 0 {
		log.Errorln("Weightwood need to more than 0 ")
		JSON(w, 400, "Weightwood need to more than 0")
		return
	}

	sm, err := FindDateSawmill(wood.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", wood.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+wood.Date)
		return
	}

	sm_total, err := FindDateTotal(wood.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", wood.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+wood.Date)
		return
	}

	checktable := 0

	for i, value_No := range sm.NoTable {

		if strings.Compare(value_No.No, wood.Notable) == 0 {
			checktable = 1
			sm.NoTable[i].WeightWood = wood.WeightWood
			sm.NoTable[i].WeightWing = wood.WeightWing
			sm.NoTable[i].PercentWing = Round(((wood.WeightWing * 100) / wood.WeightWood), 2)
			sm.NoTable[i].Remark = wood.Remark
			for j, grade := range value_No.Grade {
				for _, total := range sm_total.SumTable {
					if strings.Compare(total.No, wood.Notable) == 0 {
						for _, t_grade := range total.SumGrade {
							if strings.Compare(grade.Grade, t_grade.Grade) == 0 {
								sm.NoTable[i].Grade[j].Yield = Round(t_grade.SumFootRemain/wood.WeightWood, 3)
							}
						}
					}
				}
			}
			err = UpdateSawmill(sm)
			if err != nil {
				log.Errorln("Can't Update Table : ", sm.Date)
				JSON(w, 500, "Can't Update Table :"+sm.Date)
				return
			}
		}
	}

	if checktable == 0 {
		log.Errorln("Don't have table")
		JSON(w, 204, "Don't have table")
		return
	}

	checktable = 0

	for j, value_No := range sm_total.SumTable {
		if strings.Compare(value_No.No, wood.Notable) == 0 {
			checktable = 1
			sm_total.SumTable[j].Total.TotalYield = Round(sm_total.SumTable[j].Total.TotalFootRemain/wood.WeightWood, 3)
		}
	}

	if checktable == 0 {
		log.Errorln("Don't have table")
		JSON(w, 204, "Don't have table")
		return
	}

	err = UpdateTotalSawmill(sm_total)
	if err != nil {
		log.Errorln("Can't Update Summarize Sawmill : ", sm_total.Date)
		JSON(w, 500, "Can't Update Summarize Sawmill :"+sm_total.Date)
		return
	}
	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) NoWood(w http.ResponseWriter, r *http.Request) {
	var sm_input struct {
		Date    string `json:"date"`
		Notable string `json:"notable"`
	}

	if err := json.NewDecoder(r.Body).Decode(&sm_input); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	sm, err := FindDateSawmill(sm_input.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", sm_input.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+sm_input.Date)
		return
	}

	total, err := FindDateTotal(sm_input.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", sm_input.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+sm_input.Date)
		return
	}

	Wage := 350.00

	grade, err := FindAllDetailWood()
	if err != nil {
		log.Error("Can't find all detailwood")
		JSON(w, 404, "Can't find all detailwood")
		return
	}

	for i, no := range sm.NoTable {
		if strings.Compare(no.No, sm_input.Notable) == 0 {
			for j, em := range no.EmTable {
				sm.NoTable[i].EmTable[j].IncomeperDay = Round(em.Rate*Wage, 2)
			}
			sm.NoTable[i].Status = "false"
			sm.NoTable[i].Grade = grade
			sm.NoTable[i].WeightWing = 0
			sm.NoTable[i].WeightWood = 0
		}
	}

	err = UpdateSawmill(sm)
	if err != nil {
		log.Errorln("Can't Update Table : ", sm.Date)
		JSON(w, 500, "Can't Update Table :"+sm.Date)
		return
	}

	new_sm, err := FindDateSawmill(sm_input.Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", sm_input.Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+sm_input.Date)
		return
	}

	for _, no := range new_sm.NoTable {
		if strings.Compare(no.No, sm_input.Notable) == 0 {
			for _, grade := range no.Grade {
				err = SumWood(sm_input.Date, sm_input.Notable, grade.Grade)
				if err != nil {
					log.Errorln("Process of SumWood is error ", err)
					JSON(w, 500, "Process of SumWood is error")
					return
				}
			}
		}
	}

	for i, no := range total.SumTable {
		if strings.Compare(no.No, sm_input.Notable) == 0 {
			total.SumTable[i].Total.TotalWage = Wage
		}
		total.SumTable[i].Status = false
	}

	err = UpdateTotalSawmill(total)
	if err != nil {
		log.Errorln("Can't Update Summarize Sawmill : ", total.Date)
		JSON(w, 500, "Can't Update Summarize Sawmill :"+total.Date)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) DeleteTable(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("Day")
	No := r.Header.Get("NoTable")

	defer r.Body.Close()

	sm, err := FindDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	total, err := FindDateTotal(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}
	len := strconv.Itoa(len(sm.NoTable))

	check := 0

	for i, value_no := range sm.NoTable {

		if strings.Compare(value_no.No, No) == 0 {
			check = 1
			if No == "1" {
				sm.NoTable = sm.NoTable[i+1:]
			} else if No == len {
				sm.NoTable = sm.NoTable[:i]
			} else {
				tmp := sm.NoTable[:i]
				tmp = append(tmp, sm.NoTable[i+1:]...)
				sm.NoTable = tmp
			}
		}
	}

	if check == 0 {
		log.Println("Don't have table")
		JSON(w, 204, "Don't have table")
		return
	}

	check = 0

	for j, value_no := range total.SumTable {
		if strings.Compare(value_no.No, No) == 0 {
			check = 1
			if No == "1" {
				total.SumTable = total.SumTable[j+1:]
			} else if No == len {
				total.SumTable = total.SumTable[:j]
			} else {
				tmp := total.SumTable[:j]
				tmp = append(tmp, total.SumTable[j+1:]...)
				total.SumTable = tmp
			}
		}
	}

	if check == 0 {
		log.Println("Don't have table")
		JSON(w, 204, "Don't have table")
		return
	}

	err = UpdateSawmill(sm)
	if err != nil {
		log.Errorln("Can't Update Table : ", sm.Date)
		JSON(w, 500, "Can't Update Table :"+sm.Date)
		return
	}

	err = UpdateTotalSawmill(total)
	if err != nil {
		log.Errorln("Can't Update Summarize Sawmill : ", total.Date)
		JSON(w, 500, "Can't Update Summarize Sawmill :"+total.Date)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) DeleteDateSawmill(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("Day")

	defer r.Body.Close()

	_, err := FindDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	_, err = FindDateTotal(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill :", Date)
		JSON(w, 204, "Can't find TDate of Sawmill :"+Date)
		return
	}

	err = DelDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't delete Date of Total Sawmill :", Date)
		JSON(w, 500, "Can't delete Date of Total Sawmill :"+Date)
		return
	}

	err = DelDateTotalSawmill(Date)
	if err != nil {
		log.Errorln("Can't delete Date of Total Sawmill :", Date)
		JSON(w, 500, "Can't delete Date of Total Sawmill :"+Date)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetEmployeeinTable(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("SawmillDate")

	sm, err := FindDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	type table struct {
		Notable  string `json:"notable"`
		Employee [2]struct {
			Employeeid   string  `json:"employeeid"`
			NameTitle    string  `json:"nametitle"`
			Employee     string  `json:"employee"`
			Position     string  `json:"position"`
			Rate         float64 `json:"rate"`
			IncomeperDay float64 `json:"incomeperday"`
		}
		TotalWage float64 `json:"totalwage"`
	}

	Sm_table := make([]table, len(sm.NoTable))

	for i, value := range sm.NoTable {
		Sm_table[i].Notable = value.No
		for j, em := range value.EmTable {
			Sm_table[i].Employee[j].Employeeid = em.Employeeid
			Sm_table[i].Employee[j].NameTitle = em.NameTitle
			Sm_table[i].Employee[j].Employee = em.Employee
			Sm_table[i].Employee[j].Position = em.Position
			Sm_table[i].Employee[j].Rate = em.Rate
			Sm_table[i].Employee[j].IncomeperDay = em.IncomeperDay
			Sm_table[i].TotalWage += em.IncomeperDay
		}
	}

	res := struct {
		Table []table `json:"table"`
	}{Sm_table}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetSawmill(w http.ResponseWriter, r *http.Request) {

	//defer r.Body.Close()

	sm, err := FindAllSawmill()
	if err != nil {
		log.Errorln("Can't find All Sawmill ", err)
		JSON(w, 500, "Can't find All Sawmill")
		return
	}

	res := struct {
		Sawmill []Sawmill `json:"sawmill"`
	}{sm}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetDateSawmill(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("SawmillDate")

	sm, err := FindDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	res := struct {
		Sawmill Sawmill `json:"sawmill"`
	}{sm}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetDateTotal(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("SawmillDate")

	sm, err := FindDateTotal(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	res := struct {
		Total Total `json:"total"`
	}{sm}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetNameEmployee(w http.ResponseWriter, r *http.Request) {
	EmployeeID := r.Header.Get("Employee")

	tmpEm, _ := FindEmployee(EmployeeID)

	res := struct {
		Name string `json:"name" `
	}{tmpEm.Name + " " + tmpEm.Surname}

	JSON(w, 200, res)
	return
}

func (wd *wood) GetNameSawmill(w http.ResponseWriter, r *http.Request) {
	EmployeeID := r.Header.Get("Employee")

	tmpEm, _ := FindEmployee(EmployeeID)

	var res struct {
		Name string `json:"name"`
	}
	if tmpEm.Department == "รายวันโต๊ะเลื่อย" {
		res.Name = tmpEm.Name + " " + tmpEm.Surname
	}

	JSON(w, 200, res)

	return
}
func (wd *wood) GetTodaySawmill(w http.ResponseWriter, r *http.Request) {

	All_sm, err := FindAllSawmill()
	if err != nil {
		log.Errorln("Can't find all Sawmill ", err)
		JSON(w, 500, "Can't find all Sawmill")
		return
	}

	Previously_data := len(All_sm)

	Today := time.Now().Format("02/01/2006")

	today_sm, err := FindDateSawmill(Today)
	if err != nil {
		sm, err := FindDateSawmill(All_sm[Previously_data-1].Date)
		if err != nil {
			log.Errorln("Can't find previously Sawmill ", err)
			JSON(w, 204, "Can't find previously Sawmill")
			return
		}

		total, err := FindDateTotal(All_sm[Previously_data-1].Date)
		if err != nil {
			log.Errorln("Can't find previously Sawmill ", err)
			JSON(w, 204, "Can't find previously Sawmill")
			return
		}

		G, err := FindAllDetailWood()
		if err != nil {
			log.Error("Can't find all detailwood ", err)
			JSON(w, 500, "Can't find all detailwood")
			return
		}

		sm.Date = Today
		total.Date = Today

		for i, _ := range sm.NoTable {
			sm.NoTable[i].Grade = G
			for j, _ := range sm.NoTable[i].EmTable {
				sm.NoTable[i].EmTable[j].IncomeperDay = 0
			}
			sm.NoTable[i].WeightWood = 0
			sm.NoTable[i].WeightWing = 0
			sm.NoTable[i].PercentWing = 0
		}

		var tmpSumGrade []SumGrade

		for _, val := range G {
			tmp := SumGrade{
				Grade: val.Grade,
			}
			tmpSumGrade = append(tmpSumGrade, tmp)
		}

		for i, _ := range total.SumTable {
			total.SumTable[i].SumGrade = tmpSumGrade
			total.SumTable[i].Total.TotalAmountWood = 0
			total.SumTable[i].Total.TotalFootWood = 0
			total.SumTable[i].Total.TotalAmountBadwood = 0
			total.SumTable[i].Total.TotalFootBadwood = 0
			total.SumTable[i].Total.TotalRemain = 0
			total.SumTable[i].Total.TotalFootRemain = 0
			total.SumTable[i].Total.TotalWage = 0
			total.SumTable[i].Total.TotalYield = 0
		}

		err = InsertSawmill(sm)
		if err != nil {
			log.Errorln("Can't insert sawmill this date : ", sm.Date)
			JSON(w, http.StatusInternalServerError, "Can't insert sawmill this date : "+sm.Date)
			return
		}

		err = InsertTotalSawmill(total)
		if err != nil {
			log.Errorln("Can't insert Summarize Sawmill : ", total.Date)
			JSON(w, 500, "Can't insert Summarize Sawmill :"+total.Date)
			return
		}

		new_sm, err := FindDateSawmill(Today)
		if err != nil {
			log.Errorln("Can't find Sawmill today ", err)
			JSON(w, 204, "Can't find Sawmill today")
			return
		}

		new_total, err := FindDateTotal(Today)
		if err != nil {
			log.Errorln("Can't find Sawmill today ", err)
			JSON(w, 204, "Can't find Sawmill today")
			return
		}

		res := struct {
			DetailSawmill Sawmill `json:"detailsawmill" `
			TotalSawmill  Total   `json:"totalsawmill" `
		}{new_sm, new_total}

		JSON(w, 200, res)
	} else {
		today_total, err := FindDateTotal(Today)
		if err != nil {
			log.Errorln("Can't find Sawmill today ", err)
			JSON(w, 204, "Can't find Sawmill today")
			return
		}

		res := struct {
			DetailSawmill Sawmill `json:"detailsawmill" `
			TotalSawmill  Total   `json:"totalsawmill" `
		}{today_sm, today_total}

		JSON(w, 200, res)
	}
	return
}

func (wd *wood) SummarySawmill(w http.ResponseWriter, r *http.Request) {
	Date := r.Header.Get("SawmillDate")

	sm, err := FindDateSawmill(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	total, err := FindDateTotal(Date)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Date)
		JSON(w, 204, "Can't find Date of Sawmill : "+Date)
		return
	}

	var Total []struct {
		No         string `json:"no"`
		GroupGrade []struct {
			Grade         string  `json:"grade"`
			SumFootRemain float64 `json:"sumfootremain"`
		}
		TotalFootRemain float64 `json:"totalfootremain"`
		TotalYield      float64 `json:"totalyield"`
	}

	var tmpGroupGrade []struct {
		Grade         string  `json:"grade"`
		SumFootRemain float64 `json:"sumfootremain"`
	}

	var pallete float64

	for _, no := range total.SumTable {
		for _, grade := range no.SumGrade {
			if grade.Grade != "ขาเลทไม้แห้ง" && grade.Grade != "P" {
				tmp := struct {
					Grade         string  `json:"grade"`
					SumFootRemain float64 `json:"sumfootremain"`
				}{
					grade.Grade, grade.SumFootRemain,
				}
				tmpGroupGrade = append(tmpGroupGrade, tmp)
			} else if grade.Grade == "P" {
				pallete = grade.SumFootRemain
			} else if grade.Grade == "ขาเลทไม้แห้ง" {
				pallete += grade.SumFootRemain
				tmp := struct {
					Grade         string  `json:"grade"`
					SumFootRemain float64 `json:"sumfootremain"`
				}{
					"ไม้พาเลท", pallete,
				}
				tmpGroupGrade = append(tmpGroupGrade, tmp)
			}

		}
		tmp := struct {
			No         string `json:"no"`
			GroupGrade []struct {
				Grade         string  `json:"grade"`
				SumFootRemain float64 `json:"sumfootremain"`
			}
			TotalFootRemain float64 `json:"totalfootremain"`
			TotalYield      float64 `json:"totalyield"`
		}{no.No, tmpGroupGrade, no.Total.TotalFootRemain, no.Total.TotalYield}
		Total = append(Total, tmp)
		tmpGroupGrade = nil
	}

	var tmpGroupGrade2 []struct {
		Grade string  `json:"grade"`
		Yield float64 `json:"yield"`
	}

	var Sawmill []struct {
		No         string `json:"no"`
		GroupGrade []struct {
			Grade string  `json:"grade"`
			Yield float64 `json:"yield"`
		}
		Weight      float64 `json:"weight"`
		Wing        float64 `json:"wing"`
		PercentWing float64 `json:"percentwing"`
	}

	for _, no := range sm.NoTable {
		for _, grade := range no.Grade {
			if grade.Grade != "ขาเลทไม้แห้ง" && grade.Grade != "P" {
				tmp := struct {
					Grade string  `json:"grade"`
					Yield float64 `json:"yield"`
				}{
					grade.Grade, grade.Yield,
				}
				tmpGroupGrade2 = append(tmpGroupGrade2, tmp)
			} else if grade.Grade == "P" {
				tmp := struct {
					Grade string  `json:"grade"`
					Yield float64 `json:"yield"`
				}{
					"ไม้พาเลท", Round(pallete, 2),
				}
				tmpGroupGrade2 = append(tmpGroupGrade2, tmp)
			}

		}
		tmp := struct {
			No         string `json:"no"`
			GroupGrade []struct {
				Grade string  `json:"grade"`
				Yield float64 `json:"yield"`
			}
			Weight      float64 `json:"weight"`
			Wing        float64 `json:"wing"`
			PercentWing float64 `json:"percentwing"`
		}{no.No, tmpGroupGrade2, no.WeightWood, no.WeightWing, no.PercentWing}
		Sawmill = append(Sawmill, tmp)
		tmpGroupGrade2 = nil
	}

	var Summary struct {
		TotalWeight float64 `json:"totalweight"`
		TotalWing   float64 `json:"totalwing"`
		TotalWage   float64 `json:"totalwage"`
		AverageWing float64 `json:"averagewing"`
	}

	for _, no := range sm.NoTable {
		Summary.TotalWeight += no.WeightWood
		Summary.TotalWing += no.WeightWing
	}

	for _, no := range total.SumTable {
		Summary.TotalWage += no.Total.TotalWage
	}

	Summary.AverageWing = Round(Summary.TotalWing*100/Summary.TotalWeight, 2)

	res := struct {
		Total []struct {
			No         string `json:"no"`
			GroupGrade []struct {
				Grade         string  `json:"grade"`
				SumFootRemain float64 `json:"sumfootremain"`
			}
			TotalFootRemain float64 `json:"totalfootremain"`
			TotalYield      float64 `json:"totalyield"`
		}
		Sawmill []struct {
			No         string `json:"no"`
			GroupGrade []struct {
				Grade string  `json:"grade"`
				Yield float64 `json:"yield"`
			}
			Weight      float64 `json:"weight"`
			Wing        float64 `json:"wing"`
			PercentWing float64 `json:"percentwing"`
		}
		Summary struct {
			TotalWeight float64 `json:"totalweight"`
			TotalWing   float64 `json:"totalwing"`
			TotalWage   float64 `json:"totalwage"`
			AverageWing float64 `json:"averagewing"`
		}
	}{Total, Sawmill, Summary}
	JSON(w, 200, res)
	return
}

func (wd *wood) CalendarSawmill(w http.ResponseWriter, r *http.Request) {
	sm, err := FindAllSawmill()
	if err != nil {
		log.Errorln("Can't find all sawmill ", err)
		JSON(w, 500, "Can't find all sawmill")
		return
	}

	type Year struct {
		Year     string   `json:"year"`
		StartDay []string `json:"startday"`
		EndDay   []string `json:"endday"`
	}

	StartDay := []string{}
	EndDay := []string{}
	var calendar []Year

	for i, no := range sm {
		if i == 0 {
			StartDay = append(StartDay, no.Date)
			tmp_Date := no.Date
			num_day := strings.Index(no.Date, "/")
			tmp_Date = tmp_Date[num_day+1:]
			num_month := strings.Index(tmp_Date, "/")
			year := tmp_Date[num_month+1:]
			tmp := Year{
				Year: year,
			}
			calendar = append(calendar, tmp)
		} else {
			if no.CheckMoney == true {
				EndDay = append(EndDay, no.Date)
				if i+1 < len(sm) {
					tmp_Date := sm[i+1].Date
					num_day := strings.Index(sm[i+1].Date, "/")
					tmp_Date = tmp_Date[num_day+1:]
					num_month := strings.Index(tmp_Date, "/")
					year := tmp_Date[num_month+1:]
					if calendar[len(calendar)-1].Year != year {
						tmp := Year{
							Year: year,
						}
						calendar = append(calendar, tmp)
						StartDay = nil
						EndDay = nil
					}
					StartDay = append(StartDay, sm[i+1].Date)
					calendar[len(calendar)-1].StartDay = StartDay
				}
				calendar[len(calendar)-1].EndDay = EndDay
			}
		}
	}

	tmp := Year{
		Year: "2019",
	}
	calendar = append(calendar, tmp)
	res := struct {
		Calendar []Year `json:"calendar"`
	}{calendar}

	JSON(w, 200, res)
	return
}

func (wd *wood) SummaryCutoff(w http.ResponseWriter, r *http.Request) {
	StartDay := r.Header.Get("StartDay")
	EndDay := r.Header.Get("EndDay")

	tmp_Date := StartDay
	num_day := strings.Index(StartDay, "/")
	day, _ := strconv.Atoi(tmp_Date[:num_day])
	tmp_Date = tmp_Date[num_day+1:]
	num_month := strings.Index(tmp_Date, "/")
	month, _ := strconv.Atoi(tmp_Date[:num_month])
	year, _ := strconv.Atoi(tmp_Date[num_month+1:])

	type Daily struct {
		Date     string    `json:"date"`
		Employee []EmTable `json:"employee"`
		Total    float64   `json:"total"`
	}

	var sawmill []struct {
		No         string  `json:"no"`
		GroupDate  []Daily `json:"groupdate"`
		Total      float64 `json:"total"`
		TotalSm    float64 `json:"totalsm"`
		TotalOther float64 `json:"totalother"`
	}

	table := 1
	max_table := 0
	SmMoney := 0.0
	OtherMoney := 0.0
	AllTotal := 0.0
	var Group []Daily
	i := 0

	for {
		t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC).Add(time.Hour * time.Duration(i*24)).Format("02/01/2006")

		tmp_Daily := Daily{
			Date: t,
		}

		sm, err := FindDateSawmill(t)
		if err == nil {
			if len(sm.NoTable) > max_table {
				max_table = len(sm.NoTable)
			}
			for _, no := range sm.NoTable {
				if strings.Compare(strconv.Itoa(table), no.No) == 0 {
					tmp_Daily.Employee = no.EmTable
					for _, em := range no.EmTable {
						if no.Status == "true" {
							SmMoney += em.IncomeperDay
						} else {
							OtherMoney += em.IncomeperDay
						}
						tmp_Daily.Total += em.IncomeperDay
						AllTotal += em.IncomeperDay
					}
				}
			}
		}
		Group = append(Group, tmp_Daily)
		i++

		if t == EndDay {
			if table <= max_table {
				tmp_Sawmill := struct {
					No         string  `json:"no"`
					GroupDate  []Daily `json:"groupdate"`
					Total      float64 `json:"total"`
					TotalSm    float64 `json:"totalsm"`
					TotalOther float64 `json:"totalother"`
				}{strconv.Itoa(table), Group, SmMoney + OtherMoney, SmMoney, OtherMoney}

				sawmill = append(sawmill, tmp_Sawmill)

				Group = nil
				table++
				i = 0
				SmMoney = 0.0
				OtherMoney = 0.0
				if table == max_table {
					break
				}
			}
		}
	}

	res := struct {
		Sawmill []struct {
			No         string  `json:"no"`
			GroupDate  []Daily `json:"groupdate"`
			Total      float64 `json:"total"`
			TotalSm    float64 `json:"totalsm"`
			TotalOther float64 `json:"totalother"`
		}
		AllTotal float64 `json:"alltotal"`
	}{sawmill, AllTotal}

	JSON(w, 200, res)
	return
}

func (wd *wood) CutoffSawmill(w http.ResponseWriter, r *http.Request) {
	Today := time.Now().Format("02/01/2006")

	sm, err := FindDateSawmill(Today)
	if err != nil {
		log.Errorln("Can't find Date of Sawmill : ", Today)
		JSON(w, 204, "Can't find Date of Sawmill : "+Today)
		return
	}

	sm.CheckMoney = true

	err = UpdateSawmill(sm)
	if err != nil {
		log.Errorln("Can't Update Table : ", sm.Date)
		JSON(w, 500, "Can't Update Table :"+sm.Date)
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func Round(input float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * input
	round = math.Round(digit)
	newVal = round / pow
	return
}

func SumWood(Date string, No string, Grade string) error {

	sm, err := FindDateSawmill(Date)
	if err != nil {
		return err
	}

	total, err := FindDateTotal(Date)
	if err != nil {
		return err
	}

	SumWage := 0.0
	SumAmountWood := 0.0
	SumFootWood := 0.0
	SumAmountBadwood := 0.0
	SumFootBadwood := 0.0
	SumRemain := 0.0
	SumFootRemain := 0.0
	TotalWage := 0.0

	TotalAmountWood := 0.0
	TotalFootWood := 0.0
	TotalAmountBadwood := 0.0
	TotalFootBadwood := 0.0
	TotalRemain := 0.0
	TotalFootRemain := 0.0

	Weight := 0.0

	for i, no := range sm.NoTable {
		if No == no.No {
			for k, grade := range no.Grade {
				if grade.Grade == Grade {
					for j, detail := range grade.DetailGrade {
						SumAmountWood += detail.AmountWood
						SumFootWood += detail.FootWood
						SumAmountBadwood += detail.AmountBadwood
						SumFootBadwood += detail.FootBadwood
						SumRemain += detail.Remain
						SumFootRemain += detail.FootRemain
						SumWage += detail.Wage
						sm.NoTable[i].Grade[k].DetailGrade[j].AmountWood = Round(detail.AmountWood, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].FootWood = Round(detail.FootWood, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].AmountBadwood = Round(detail.AmountBadwood, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].FootBadwood = Round(detail.FootBadwood, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].Remain = Round(detail.Remain, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].FootRemain = Round(detail.FootRemain, 2)
						sm.NoTable[i].Grade[k].DetailGrade[j].Wage = Round(detail.Wage, 2)
					}
					if no.WeightWood > 0 {
						sm.NoTable[i].Grade[k].Yield = Round(SumFootRemain/no.WeightWood, 3)
						Weight = no.WeightWood
					}
				}
			}
		}
	}

	for i, no := range total.SumTable {
		if no.No == No {
			for j, grade := range no.SumGrade {
				if grade.Grade == Grade {
					total.SumTable[i].SumGrade[j].SumAmountWood = SumAmountWood
					total.SumTable[i].SumGrade[j].SumFootWood = SumFootWood
					total.SumTable[i].SumGrade[j].SumAmountBadwood = SumAmountBadwood
					total.SumTable[i].SumGrade[j].SumFootBadwood = SumFootBadwood
					total.SumTable[i].SumGrade[j].SumRemain = SumRemain
					total.SumTable[i].SumGrade[j].SumFootRemain = SumFootRemain
					total.SumTable[i].SumGrade[j].SumWage = SumWage
				}
			}
			total.SumTable[i].Status = true
		}
	}

	err = UpdateTotalSawmill(total)
	if err != nil {
		return err
	}

	err = UpdateSawmill(sm)
	if err != nil {
		return err
	}

	new_sm, err := FindDateSawmill(Date)
	if err != nil {
		return err
	}

	new_total, err := FindDateTotal(Date)
	if err != nil {
		return err
	}

	for i, no := range new_total.SumTable {
		if no.No == No {
			for j, grade := range no.SumGrade {
				TotalAmountWood += grade.SumAmountWood
				TotalFootWood += grade.SumFootWood
				TotalAmountBadwood += grade.SumAmountBadwood
				TotalFootBadwood += grade.SumFootBadwood
				TotalRemain += grade.SumRemain
				TotalFootRemain += grade.SumFootRemain
				TotalWage += grade.SumWage
				new_total.SumTable[i].SumGrade[j].SumAmountWood = Round(grade.SumAmountWood, 2)
				new_total.SumTable[i].SumGrade[j].SumFootWood = Round(grade.SumFootWood, 2)
				new_total.SumTable[i].SumGrade[j].SumAmountBadwood = Round(grade.SumAmountBadwood, 2)
				new_total.SumTable[i].SumGrade[j].SumFootBadwood = Round(grade.SumFootBadwood, 2)
				new_total.SumTable[i].SumGrade[j].SumRemain = Round(grade.SumRemain, 2)
				new_total.SumTable[i].SumGrade[j].SumFootRemain = Round(grade.SumFootRemain, 2)
				new_total.SumTable[i].SumGrade[j].SumWage = Round(grade.SumWage, 2)
			}
			new_total.SumTable[i].Total.TotalYield = Round(TotalFootRemain/Weight, 3)
			new_total.SumTable[i].Total.TotalAmountWood = Round(TotalAmountWood, 2)
			new_total.SumTable[i].Total.TotalFootWood = Round(TotalFootWood, 2)
			new_total.SumTable[i].Total.TotalAmountBadwood = Round(TotalAmountBadwood, 2)
			new_total.SumTable[i].Total.TotalFootBadwood = Round(TotalFootBadwood, 2)
			new_total.SumTable[i].Total.TotalRemain = Round(TotalRemain, 2)
			new_total.SumTable[i].Total.TotalFootRemain = Round(TotalFootRemain, 2)
			new_total.SumTable[i].Total.TotalWage = Round(TotalWage, 2)
		}
	}

	for i, no := range new_sm.NoTable {
		if no.No == No {
			for j, em := range no.EmTable {
				new_sm.NoTable[i].EmTable[j].IncomeperDay = Round(em.Rate*TotalWage, 2)
			}
		}
	}

	err = UpdateTotalSawmill(new_total)
	if err != nil {
		return err
	}
	err = UpdateSawmill(new_sm)
	if err != nil {
		return err
	}
	return nil
}
