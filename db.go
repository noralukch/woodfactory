package main

import (
	log "github.com/sirupsen/logrus"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var db *mgo.Database

func init() {
	s, err := mgo.Dial("localhost")
	if err != nil {
		log.Fatalln(err)
		return
	}

	db = s.DB("WoodFactory")

}

func FindUser(Username string) (User, error) {
	var user User
	err := db.C("User").Find(bson.M{"employeeid": Username}).One(&user)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func FindAllUser() ([]User, error) {
	var user []User
	err := db.C("User").Find(nil).All(&user)
	if err != nil {
		return []User{}, err
	}
	return user, nil
}

func InsertUser(us User) error {
	err := db.C("User").Insert(us)
	if err != nil {
		return err
	}
	return nil
}

func UpdateUser(us User) error {
	var tmpus User
	if err := db.C("User").Find(bson.M{"employeeid": us.EmployeeID}).One(&tmpus); err != nil {
		log.Errorln("Can't find User : ", us.EmployeeID)
		return err
	}
	if err := db.C("User").Update(bson.M{"employeeid": us.EmployeeID}, &us); err != nil {
		log.Errorln("Can't update User : ", us.EmployeeID)
		return err
	}
	return nil
}

func DelUser(UserID string) error {
	var us User
	if err := db.C("User").Find(bson.M{"employeeid": UserID}).One(&us); err != nil {
		log.Errorln("Can't find User : ", UserID)
		return err
	}
	if err := db.C("User").Remove(bson.M{"employeeid": UserID}); err != nil {
		return err
	}
	return nil
}

func InsertDepartment(dm Department) error {
	err := db.C("Department").Insert(dm)
	if err != nil {
		return err
	}
	return nil
}

func InsertEmployee(em Employee) error {
	err := db.C("Employee").Insert(em)
	if err != nil {
		return err
	}
	return nil
}

func InsertQuantity(em QuantityEmployee) error {
	err := db.C("Quantity").Insert(em)
	if err != nil {
		return err
	}
	return nil
}

func FindDepartment() ([]Department, error) {
	var dm []Department
	if err := db.C("Department").Find(nil).All(&dm); err != nil {
		log.Errorln("Can't find Department ")
		return []Department{}, err
	}
	return dm, nil
}

func FindOneDepartment(name string) (Department, error) {
	var dm Department
	if err := db.C("Department").Find(bson.M{"name": name}).One(&dm); err != nil {
		//log.Errorln("Can't find Department ")
		return Department{}, err
	}
	return dm, nil
}

func FindOneDepartmentbyStringID(id string) (Department, error) {
	var dm Department
	if err := db.C("Department").Find(bson.M{"_id": id}).One(&dm); err != nil {
		//log.Errorln("Can't find Department ")
		return Department{}, err
	}
	return dm, nil
}

func FindOneDepartmentbyID(id bson.ObjectId) (Department, error) {
	var dm Department
	if err := db.C("Department").Find(bson.M{"_id": id}).One(&dm); err != nil {
		log.Errorln("Can't find Department ")
		return Department{}, err
	}
	return dm, nil
}

func UpdateDepartment(dm Department) error {

	if err := db.C("Department").Update(bson.M{"name": dm.Name}, &dm); err != nil {
		log.Errorln("Can't update Department : ", dm.Name)
		return err
	}
	return nil
}

func UpdateDepartmentbyID(dm Department) error {

	if err := db.C("Department").Update(bson.M{"_id": dm.ID}, &dm); err != nil {
		log.Errorln("Can't update Department : ", dm.Name)
		return err
	}
	return nil
}

func DelDepartment(name string) error {

	if err := db.C("Department").Remove(bson.M{"name": name}); err != nil {
		log.Errorln("Can't remove Department : ", name)
		return err
	}
	return nil
}

func FindEmployee(EmID string) (Employee, error) {
	var em Employee
	if err := db.C("Employee").Find(bson.M{"employeeid": EmID}).One(&em); err != nil {
		return Employee{}, err
	}
	return em, nil
}

func FindQuantityinYear(Year string) (QuantityEmployee, error) {
	var em QuantityEmployee
	if err := db.C("Quantity").Find(bson.M{"year": Year}).One(&em); err != nil {
		return QuantityEmployee{}, err
	}
	return em, nil
}

func UpdateQuantity(dm QuantityEmployee) error {

	if err := db.C("Quantity").Update(bson.M{"year": dm.Year}, &dm); err != nil {
		log.Errorln("Can't update Quantity in year : ", dm.Year)
		return err
	}
	return nil
}

func FindFullNameEmployee(name string, surname string) (Employee, error) {
	var em Employee
	if err := db.C("Employee").Find(bson.M{"name": name, "surname": surname}).One(&em); err != nil {
		return Employee{}, err
	}
	return em, nil
}

func FindNameEmployee(name string) (Employee, error) {
	var em Employee
	if err := db.C("Employee").Find(bson.M{"name": name}).One(&em); err != nil {
		return Employee{}, err
	}
	return em, nil
}

func FindSurnameEmployee(surname string) ([]Employee, error) {
	var em []Employee
	if err := db.C("Employee").Find(bson.M{"surname": surname}).All(&em); err != nil {
		return []Employee{}, err
	}
	return em, nil
}

func FindDepartmentEmployee(department string) (Employee, error) {
	var em Employee
	if err := db.C("Employee").Find(bson.M{"department": department}).One(&em); err != nil {
		return Employee{}, err
	}
	return em, nil
}

func FindEmployeeAll() ([]Employee, error) {
	var em []Employee
	if err := db.C("Employee").Find(nil).All(&em); err != nil {
		log.Errorln("Can't find Employee ")
		return []Employee{}, err
	}
	return em, nil
}

func UpdateEmployee(em Employee) error {
	var tmpem Employee
	if err := db.C("Employee").Find(bson.M{"employeeid": em.EmployeeID}).One(&tmpem); err != nil {
		log.Errorln("Can't find Employee : ", em.EmployeeID)
		return err
	}
	if err := db.C("Employee").Update(bson.M{"employeeid": em.EmployeeID}, &em); err != nil {
		log.Errorln("Can't update Employee : ", em.EmployeeID)
		return err
	}
	return nil
}

func DelEmployee(EmID string) error {
	var em Employee
	if err := db.C("Employee").Find(bson.M{"employeeid": EmID}).One(&em); err != nil {
		log.Errorln("Can't find Employee : ", em.EmployeeID)
		return err
	}
	if err := db.C("Employee").Remove(bson.M{"employeeid": EmID}); err != nil {
		log.Errorln("Can't remove Employee : ", em.EmployeeID)
		return err
	}
	return nil
}

func InsertWoodDB(wd WoodStorage) error {
	err := db.C("WoodStorage").Insert(wd)
	if err != nil {
		return err
	}
	return nil
}

func FindWood() ([]WoodStorage, error) {
	var wd []WoodStorage
	if err := db.C("WoodStorage").Find(nil).All(&wd); err != nil {
		log.Errorln("Can't find Wood ")
		return []WoodStorage{}, err
	}
	return wd, nil
}

func InsertTotalSawmill(total Total) error {
	err := db.C("TotalSawmill").Insert(total)
	if err != nil {
		return err
	}
	return nil
}

func InsertSawmill(sm Sawmill) error {
	err := db.C("Sawmill").Insert(sm)
	if err != nil {
		return err
	}
	return nil
}

func FindDateTotal(date string) (Total, error) {
	var total Total
	if err := db.C("TotalSawmill").Find(bson.M{"date": date}).One(&total); err != nil {
		return Total{}, err
	}
	return total, nil
}

func FindDateSawmill(date string) (Sawmill, error) {
	var sm Sawmill
	if err := db.C("Sawmill").Find(bson.M{"date": date}).One(&sm); err != nil {
		return Sawmill{}, err
	}
	return sm, nil
}

func FindAllSawmill() ([]Sawmill, error) {
	var sm []Sawmill
	if err := db.C("Sawmill").Find(nil).All(&sm); err != nil {
		log.Errorln("Can't find Sawmill ")
		return []Sawmill{}, err
	}
	return sm, nil
}

func FindAllTotalSawmill() ([]Total, error) {
	var tb []Total
	if err := db.C("TotalSawmill").Find(nil).All(&tb); err != nil {
		log.Errorln("Can't find Total Sawmill ")
		return []Total{}, err
	}
	return tb, nil
}

func UpdateSawmill(sm Sawmill) error {

	if err := db.C("Sawmill").Update(bson.M{"date": sm.Date}, &sm); err != nil {
		return err
	}
	return nil
}

func UpdateTotalSawmill(total Total) error {

	if err := db.C("TotalSawmill").Update(bson.M{"date": total.Date}, &total); err != nil {
		return err
	}
	return nil
}

func DelDateTotalSawmill(date string) error {
	if err := db.C("TotalSawmill").Remove(bson.M{"date": date}); err != nil {
		log.Errorln("Can't remove TotalSawmill at date: ", date)
		return err
	}
	return nil
}

func DelDateSawmill(date string) error {
	if err := db.C("Sawmill").Remove(bson.M{"date": date}); err != nil {
		log.Errorln("Can't remove Sawmill at date : ", date)
		return err
	}
	return nil
}

func InsertDetailWood(sm Grade) error {
	err := db.C("DetailWood").Insert(sm)
	if err != nil {
		return err
	}
	return nil
}

func FindDetailWood(grade string) (Grade, error) {
	var g Grade
	if err := db.C("DetailWood").Find(bson.M{"grade": grade}).One(&g); err != nil {
		return Grade{}, err
	}
	return g, nil
}

func FindAllDetailWood() ([]Grade, error) {
	var tb []Grade
	if err := db.C("DetailWood").Find(nil).All(&tb); err != nil {
		return []Grade{}, err
	}
	return tb, nil
}

func UpdateDetailWood(g Grade) error {

	if err := db.C("DetailWood").Update(bson.M{"grade": g.Grade}, &g); err != nil {
		return err
	}
	return nil
}

func DelDetailWood(grade string) error {
	if err := db.C("DetailWood").Remove(bson.M{"grade": grade}); err != nil {
		return err
	}
	return nil
}

func InsertAuthEmployee(auth AuthEmployee) error {
	err := db.C("Auth").Insert(auth)
	if err != nil {
		return err
	}
	return nil
}

func UpdateAuthEmployee(auth AuthEmployee) error {

	if err := db.C("Auth").Update(bson.M{"employeeid": auth.EmployeeID}, &auth); err != nil {
		return err
	}
	return nil
}

func FindAllAuthEmployee() ([]AuthEmployee, error) {
	var auth []AuthEmployee
	if err := db.C("Auth").Find(nil).All(&auth); err != nil {
		return []AuthEmployee{}, err
	}
	return auth, nil
}

func FindAuthEmployee(auth string) (AuthEmployee, error) {
	var tmpAuth AuthEmployee
	if err := db.C("Auth").Find(bson.M{"employeeid": auth}).One(&tmpAuth); err != nil {
		return AuthEmployee{}, err
	}
	return tmpAuth, nil
}

func DeleteAuthEmployee(auth string) error {
	if err := db.C("Auth").Remove(bson.M{"employeeid": auth}); err != nil {
		return err
	}
	return nil
}
