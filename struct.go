package main

import (
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	EmployeeID string        `bson:"employeeid"`
	Salt       string        `bson:"Salt"`
	Hash       string        `bson:"Hash"`
	Role       string        `bson:"role"`
}

type Employee struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	EmployeeID  string        `bson:"employeeid"`
	NameTitle   string        `bson:"nametitle"`
	Name        string        `bson:"name"`
	Surname     string        `bson:"surname"`
	Nickname    string        `bson:"nickname"`
	Gender      string        `bson:"gender"`
	Age         int           `bson:"age"`
	National    string        `bson:"national"`
	Tel         string        `bson:"tel"`
	Address     string        `bson:"address"`
	Department  string        `bson:"department"`
	Position    string        `bson:"position"`
	SalaryType  string        `bson:"salarytype"`
	StartWork   string        `bson:"startwork"`
	PicturePath []string      `bson:"picturepath"`
	Email       string        `bson:"email"`
}

type AuthEmployee struct {
	EmployeeID string `bson:"employeeid"`
	Date       string `bson:"date"`
	Status     bool   `bson:"status"`
}

type QuantityEmployee struct {
	Year     string `bson:"year"`
	Quantity int    `bson:"quantity"`
}

type Department struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Name      string        `bson:"name"`
	ManagerID string        `bson:"managerid"`
	Manager   string        `bson:"manager"`
	Employee  []string      `bson:"employee"`
}

type tmp struct {
	Employee []string `bson:"employee"`
}

type WoodStorage struct {
	ID   bson.ObjectId `bson:"_id,omitempty"`
	Date string        `bson:"date"`
	//Type       string        `bson:"type"`
	WeightWood float64 `bson:"weightwood"`
	Remain     float64 `bson:"remain"`
	//CostWood   float64       `bson:"costwood"`
	Total float64 `bson:"total"`
}

type Sawmill struct {
	Date       string    `bson:"date"`
	NoTable    []Notable `bson:"notable"`
	CheckMoney bool      `bson:"checkmoney"`
}

type Notable struct {
	No          string    `bson:"no"`
	Grade       []Grade   `bson:"grade"`
	EmTable     []EmTable `bson:"emtable"`
	WeightWood  float64   `bson:"weightwood"`
	WeightWing  float64   `bson:"weightwing"`
	PercentWing float64   `bson:"percentwing"`
	Remark      string    `bson:"remark"`
	Status      string    `bson:"status"`
}

type Table struct {
	Notable string    `bson:"notable"`
	EmTable []EmTable `bson:"emtable"`
}

type EmTable struct {
	Employeeid   string  `bson:"employeeid"`
	NameTitle    string  `bson:"nametitle"`
	Employee     string  `bson:"employee"`
	Position     string  `bson:"position"`
	Rate         float64 `bson:"rate"`
	IncomeperDay float64 `bson:"incomeperday"`
}

type Grade struct {
	Grade       string        `bson:"grade"`
	Yield       float64       `bson:"yield"`
	DetailGrade []DetailGrade `bson:"detailgrade"`
}

type DetailGrade struct {
	Size          string  `bson:"size"`
	AmountWood    float64 `bson:"amountwood"`
	FootWood      float64 `bson:"footwood"`
	AmountBadwood float64 `bson:"amountbadwood"`
	FootBadwood   float64 `bson:"footbadwood"`
	Remain        float64 `bson:"remain"`
	FootRemain    float64 `bson:"footremain"`
	Wage          float64 `bson:"wage"`
	CostWood      float64 `bson:"costwood"`
}

type Total struct {
	Date     string     `bson:"date"`
	SumTable []SumTable `bson:"sumtable"`
}

type SumTable struct {
	No       string     `bson:"no"`
	SumGrade []SumGrade `bson:"sumgrade"`
	Total    struct {
		TotalAmountWood    float64 `bson:"totalamountwood"`
		TotalFootWood      float64 `bson:"totalfootwood"`
		TotalAmountBadwood float64 `bson:"totalamountbadwood"`
		TotalFootBadwood   float64 `bson:"totalfootbadwood"`
		TotalRemain        float64 `bson:"totalremain"`
		TotalFootRemain    float64 `bson:"totalfootremain"`
		TotalWage          float64 `bson:"totalwage"`
		TotalYield         float64 `bson:"totalyield"`
	}
	Status bool `bson:"status"`
}

type SumGrade struct {
	Grade            string  `bson:"grade"`
	SumAmountWood    float64 `bson:"sumamountwood"`
	SumFootWood      float64 `bson:"sumfootwood"`
	SumAmountBadwood float64 `bson:"sumamountbadwood"`
	SumFootBadwood   float64 `bson:"sumfootbadwood"`
	SumRemain        float64 `bson:"sumremain"`
	SumFootRemain    float64 `bson:"sumfootremain"`
	SumWage          float64 `bson:"sumwage"`
}
