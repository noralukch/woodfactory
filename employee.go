package main

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

func (wd *wood) InsertEmployee(w http.ResponseWriter, r *http.Request) {
	var em struct {
		NameTitle  string `json:"nametitle"`
		Name       string `json:"name"`
		Surname    string `json:"surname"`
		Nickname   string `json:"nickname"`
		Gender     string `json:"gender"`
		Age        int    `json:"age"`
		National   string `json:"national"`
		Tel        string `json:"tel"`
		Address    string `json:"address"`
		Department string `json:"department"`
		Position   string `json:"position"`
		SalaryType string `json:"salarytype"`
		Email      string `json:"email"`
	}

	//log.Println("r : ", r.FormValue("Employee"))
	err := json.Unmarshal([]byte(r.FormValue("Employee")), &em)
	if err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusBadRequest, err)
		return
	}

	Year := time.Now().Format("06")

	if len(em.Name) == 0 || len(em.Address) == 0 || len(em.Tel) == 0 || len(em.Surname) == 0 || em.Gender == "--" || em.Age <= 0 || len(em.Department) == 0 || len(em.SalaryType) == 0 || em.National == "--" {
		log.Errorln("Please enter your information")
		JSON(w, 400, "Please enter your information")
		return
	}

	_, err = FindQuantityinYear(Year)
	if err != nil {
		tmpYear := QuantityEmployee{
			Year:     Year,
			Quantity: 1,
		}
		err = InsertQuantity(tmpYear)
		if err != nil {
			log.Error("Can't insert Year", err)
			JSON(w, 500, "Can't insert Year")
			return
		}
	}

	year, err := FindQuantityinYear(Year)
	if err != nil {
		log.Errorln("Can't find year : ", Year)
		JSON(w, 204, "Can't find year : "+Year)
		return
	}

	var EmployeeID string

	if year.Quantity >= 0 && year.Quantity <= 9 {
		EmployeeID = Year + "000" + strconv.Itoa(year.Quantity)
		year.Quantity = year.Quantity + 1
	} else if year.Quantity >= 10 && year.Quantity <= 99 {
		EmployeeID = Year + "00" + strconv.Itoa(year.Quantity)
		year.Quantity = year.Quantity + 1
	} else if year.Quantity >= 100 && year.Quantity <= 999 {
		EmployeeID = Year + "0" + strconv.Itoa(year.Quantity)
		year.Quantity = year.Quantity + 1
	} else if year.Quantity >= 1000 && year.Quantity <= 9999 {
		EmployeeID = Year + strconv.Itoa(year.Quantity)
		year.Quantity = year.Quantity + 1
	}

	_, err = FindFullNameEmployee(em.Name, em.Surname)
	if err == nil {
		log.Errorln("Name & Surname was use ", err)
		JSON(w, 400, "Name & Surname was use")
		return
	}

	dm, err := FindOneDepartment(em.Department)
	if err != nil {
		log.Errorln("This department don't have ", err)
		JSON(w, 204, "This department don't have")
		return
	}

	dm.Employee = append(dm.Employee, EmployeeID)

	if dm.Manager != "" && em.Position == "หัวหน้าแผนก" {
		log.Errorln("This Department had a manager")
		JSON(w, 400, "This Department had a manager")
		return
	}

	err = os.MkdirAll("/var/asset/employee/"+EmployeeID, 0777)
	if err != nil {
		log.Errorln("Can't Mkdir : ", err)
		JSON(w, 500, err)
		return
	}
	r.ParseMultipartForm(32 << 20)
	PictureEmployee := r.MultipartForm.File["PictureEmployee"]

	defer r.Body.Close()
	var tmpPath []string
	for i := range PictureEmployee {
		destination, err := os.OpenFile("/var/asset/employee/"+EmployeeID+"/"+PictureEmployee[i].Filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0755)
		if err != nil {
			log.Errorln("Open to create error ", err)
			JSON(w, 500, "Open to create error")
			return
		}

		tmpPic, err := PictureEmployee[i].Open()
		if err != nil {
			log.Errorln("Can't open picture ", err)
			JSON(w, 400, "Can't open picture")
			return
		}
		_, err = io.Copy(destination, tmpPic)
		if err != nil {
			log.Errorln("Copy error ", err)
			JSON(w, 500, "Copy error")
			return
		}
		tmpPath = append(tmpPath, "/"+EmployeeID+"/"+PictureEmployee[i].Filename)
	}

	tmpEmployee := Employee{
		EmployeeID:  EmployeeID,
		NameTitle:   em.NameTitle,
		Name:        em.Name,
		Surname:     em.Surname,
		Nickname:    em.Nickname,
		Gender:      em.Gender,
		Age:         em.Age,
		National:    em.National,
		Tel:         em.Tel,
		Address:     em.Address,
		Department:  em.Department,
		Position:    em.Position,
		SalaryType:  em.SalaryType,
		StartWork:   time.Now().Format("02/01/2006"),
		PicturePath: tmpPath,
		Email:       em.Email,
	}

	err = InsertEmployee(tmpEmployee)
	if err != nil {
		log.Errorln("Can't insert Employee ", err)
		JSON(w, 500, "Can't insert Employee")
		return
	}

	if em.Position == "หัวหน้าแผนก" {
		dm.ManagerID = EmployeeID
		dm.Manager = em.NameTitle + " " + em.Name + " " + em.Surname
	}

	err = UpdateDepartmentbyID(dm)
	if err != nil {
		log.Error("Can't update employee in department ", err)
		JSON(w, 500, "Can't update employee in department")
		return
	}

	err = UpdateQuantity(year)
	if err != nil {
		log.Error("Can't update quantity in Year : ", err)
		JSON(w, 500, "Can't update quantity in Year")
		return
	}

	//log.Println(tmpEmployee)
	res := struct {
		EmployeeID string `json:"employeeid" `
	}{EmployeeID}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	var em struct {
		EmployeeID string `json:"employeeid"`
		NameTitle  string `json:"nametitle"`
		Name       string `json:"name"`
		Surname    string `json:"surname"`
		Nickname   string `json:"nickname"`
		Gender     string `json:"gender"`
		Age        int    `json:"age"`
		National   string `json:"national"`
		Tel        string `json:"tel"`
		Address    string `json:"address"`
		Department string `json:"department"`
		Position   string `json:"position"`
		SalaryType string `json:"salarytype"`
		Email      string `json:"email"`
	}
	err := json.Unmarshal([]byte(r.FormValue("Employee")), &em)
	if err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusBadRequest, err)
		return
	}

	if len(em.Name) == 0 || len(em.Address) == 0 || len(em.Tel) == 0 || len(em.Surname) == 0 || em.Gender == "--" || em.Age <= 0 || len(em.Department) == 0 || len(em.SalaryType) == 0 || em.National == "--" {
		log.Errorln("Please enter your information")
		JSON(w, 400, "Please enter your information")
		return
	}

	old_em, err := FindEmployee(em.EmployeeID)
	if err != nil {
		log.Errorln("Can't fine this employee : ", em.EmployeeID)
		JSON(w, 204, "Can't fine this employee : "+em.EmployeeID)
		return
	}

	fullname, err := FindFullNameEmployee(em.Name, em.Surname)
	if err == nil {
		if fullname.EmployeeID != em.EmployeeID {
			log.Errorln("Name & Surname was use", err)
			JSON(w, 400, "Name & Surname was use")
			return
		}
	}

	if old_em.Department != em.Department {

		old_dm, err := FindOneDepartment(old_em.Department)
		if err != nil {
			log.Errorln("This department don't have", err)
			JSON(w, 204, "This department don't have")
			return
		}

		new_dm, err := FindOneDepartment(em.Department)
		if err != nil {
			log.Errorln("This department don't have", err)
			JSON(w, 204, "This department don't have")
			return
		}

		for i, num_em := range old_dm.Employee {
			if em.EmployeeID == num_em {
				tmpEM := old_dm.Employee[:i]
				tmpEM = append(tmpEM, old_dm.Employee[i+1:]...)

				old_dm.Employee = tmpEM
				break
			}
		}

		if old_em.Position == "หัวหน้าแผนก" {
			old_dm.ManagerID = ""
			old_dm.Manager = ""
		}

		err = UpdateDepartment(old_dm)
		if err != nil {
			log.Errorln("Can't Update old department", err)
			JSON(w, 500, "Can't Update old department")
			return
		}

		new_dm.Employee = append(new_dm.Employee, em.EmployeeID)

		if em.Position == "หัวหน้าแผนก" {
			if new_dm.ManagerID != "" {
				// num := strings.Index(new_dm.Manager, " ")
				// first := new_dm.Manager[:num]
				// second := new_dm.Manager[num+1:]
				old_manager, err := FindEmployee(new_dm.ManagerID)
				if err != nil {
					log.Errorln("Can't fine this employee : ", new_dm.ManagerID)
					JSON(w, 204, "Can't fine this employee : "+new_dm.ManagerID)
					return
				}
				old_manager.Position = ""
				err = UpdateEmployee(old_manager)
				if err != nil {
					log.Errorln("Can't update old manager  : ", err)
					JSON(w, 500, "Can't update old manager ")
					return
				}
			}
			new_dm.ManagerID = em.EmployeeID
			new_dm.Manager = em.NameTitle + " " + em.Name + " " + em.Surname
		}

		err = UpdateDepartment(new_dm)
		if err != nil {
			log.Errorln("Can't Update new department ", err)
			JSON(w, 500, "Can't Update new department")
			return
		}

	}

	r.ParseMultipartForm(32 << 20)
	PictureEmployee := r.MultipartForm.File["PictureEmployee"]

	if len(PictureEmployee) > 0 {
		if len(old_em.PicturePath) > 0 {
			_ = os.Remove("/var/asset/employee/" + em.EmployeeID + "/" + old_em.PicturePath[0])

			err = os.MkdirAll("/var/asset/employee/"+em.EmployeeID, 0777)
			if err != nil {
				log.Errorln(err)
				JSON(w, 500, err)
				return
			}
		} else {
			err = os.MkdirAll("/var/asset/employee/"+em.EmployeeID, 0777)
			if err != nil {
				log.Errorln(err)
				JSON(w, 500, err)
				return
			}
		}

	}

	var tmpPath []string
	for i := range PictureEmployee {
		destination, err := os.OpenFile("/var/asset/employee/"+em.EmployeeID+"/"+PictureEmployee[i].Filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0755)
		if err != nil {
			log.Errorln("Open to create file ", err)
			JSON(w, 500, "Open to create file ")
			return
		}
		tmpPic, err := PictureEmployee[i].Open()
		if err != nil {
			log.Errorln("Can't open picture ", err)
			JSON(w, 400, "Can't open picture")
			return
		}
		_, err = io.Copy(destination, tmpPic)
		if err != nil {
			log.Errorln("Copy error ", err)
			JSON(w, 500, "Copy error")
			return
		}
		tmpPath = append(tmpPath, "/"+em.EmployeeID+"/"+PictureEmployee[i].Filename)
	}

	if len(PictureEmployee) > 0 {

		tmpEmployee := Employee{
			EmployeeID:  em.EmployeeID,
			NameTitle:   em.NameTitle,
			Name:        em.Name,
			Surname:     em.Surname,
			Nickname:    em.Nickname,
			Gender:      em.Gender,
			Age:         em.Age,
			Tel:         em.Tel,
			National:    em.National,
			StartWork:   old_em.StartWork,
			Address:     em.Address,
			Department:  em.Department,
			Position:    em.Position,
			SalaryType:  em.SalaryType,
			PicturePath: tmpPath,
			Email:       em.Email,
		}
		err = UpdateEmployee(tmpEmployee)
		if err != nil {
			log.Errorln("Can't update Employee : ", err)
			JSON(w, 500, "Can't update Employee")
			return
		}
	} else {

		tmpEmployee := Employee{
			EmployeeID:  em.EmployeeID,
			NameTitle:   em.NameTitle,
			Name:        em.Name,
			Surname:     em.Surname,
			Nickname:    em.Nickname,
			Gender:      em.Gender,
			Age:         em.Age,
			Tel:         em.Tel,
			National:    em.National,
			StartWork:   old_em.StartWork,
			Address:     em.Address,
			Department:  em.Department,
			Position:    em.Position,
			SalaryType:  em.SalaryType,
			PicturePath: old_em.PicturePath,
			Email:       em.Email,
		}
		err = UpdateEmployee(tmpEmployee)
		if err != nil {
			log.Errorln("Can't update Employee", err)
			JSON(w, 500, "Can't update Employee")
			return
		}
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	EmployeeID := r.Header.Get("EmployeeID")

	defer r.Body.Close()

	detail_em, err := FindEmployee(EmployeeID)
	if err != nil {
		log.Errorln("Can't Find employee : ", EmployeeID)
		JSON(w, 204, "Can't Find employee : "+EmployeeID)
		return
	}

	dm, err := FindOneDepartment(detail_em.Department)
	if err != nil {
		log.Errorln("This department don't have ", err)
		JSON(w, 204, "This department don't have")
		return
	}

	var length int

	for i, num_em := range dm.Employee {
		if EmployeeID == num_em {
			length = i
			break
		}
	}

	tmpEM := dm.Employee[:length]
	tmpEM = append(tmpEM, dm.Employee[length+1:]...)

	dm.Employee = tmpEM

	if detail_em.Position == "หัวหน้าแผนก" {
		dm.ManagerID = ""
		dm.Manager = ""
	}

	err = UpdateDepartment(dm)
	if err != nil {
		log.Errorln("Can't Update old department ", err)
		JSON(w, 500, "Can't Update old department")
		return
	}

	err = DelEmployee(EmployeeID)
	if err != nil {
		log.Errorln("Delete employee error: ", err)
		JSON(w, 500, "Delelte employee error ")
		return
	}

	err = os.RemoveAll("/var/asset/employee/" + EmployeeID)
	if err != nil {
		log.Errorln(err)
		JSON(w, 500, err)
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) GetEmployee(w http.ResponseWriter, r *http.Request) {

	//defer r.Body.Close()

	em, err := FindEmployeeAll()
	if err != nil {
		log.Errorln("Can't Get All Employee ", err)
		JSON(w, 500, "Can't Get All Employee ")
		return
	}

	res := struct {
		Employee []Employee `json:"employee" `
	}{em}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) GetOneEmployee(w http.ResponseWriter, r *http.Request) {
	EmployeeID := r.Header.Get("Employee")

	profile, err := FindEmployee(EmployeeID)
	if err != nil {
		log.Errorln("Can't find Employee  " + EmployeeID)
		JSON(w, 204, "Can't find Employee "+EmployeeID)
		return
	}

	res := struct {
		Employee Employee `json:"employee" `
	}{profile}

	JSON(w, http.StatusOK, res)
	return
}
