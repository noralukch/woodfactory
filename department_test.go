package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_wood_InsertDepartment(t *testing.T) {
	data := []struct {
		Name string
	}{
		{
			"A",
		},
		{
			"",
		},
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/insertdepartment", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Have data",
			testW,
			args{w, req[0]},
		},
		{
			"Not have data",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.InsertDepartment(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetDepartment(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/getdepartment", nil)
	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"GetDepartment",
			testW,
			args{w, req},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetDepartment(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetDepartmentDetail(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Name string
	}{
		{
			"",
		},
		{
			"CZC",
		},
		{
			"QC",
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/insertdepartment", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"don't have input",
			testW,
			args{w, req[0]},
		},
		{
			"don't have department",
			testW,
			args{w, req[1]},
		},
		{
			"GetDepartmentDetail Success",
			testW,
			args{w, req[2]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetDepartmentDetail(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_DeleteDepartment(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()

	Name := []string{"5b7ff4cd45956984fa56b0f4", "5b755012f24b0914d9e6f108"}

	var req []*http.Request

	for i, _ := range Name {
		r := httptest.NewRequest("DELETE", "/deletedepartment", nil)
		r.Header.Set("ID", Name[i])
		req = append(req, r)
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Have employee in department",
			testW,
			args{w, req[0]},
		},
		{
			"Don't have this department",
			testW,
			args{w, req[1]},
		},
		// {
		// 	"Department don't have employee",
		// 	testW,
		// 	args{w, req[2]},
		// 	200,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.DeleteDepartment(tt.args.w, tt.args.r)
		})

	}
}

func Test_wood_UpdateDepartment(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		ID   string
		Name string
	}{
		{
			"",
			"",
		},
		{
			"5b754fa5f24b0914d9e6f0fa",
			"B",
		},
		{
			"5b754fa5f24b0914d9e6f0fc",
			"B",
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("PUT", "/updatedepartment", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"No input",
			testW,
			args{w, req[0]},
		},
		{
			"Department wrong",
			testW,
			args{w, req[1]},
		},
		// {
		// 	"Update Department Success",
		// 	testW,
		// 	args{w, req[2]},
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.UpdateDepartment(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_ChangeDepartmentEmployee(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data2 := [][]struct {
		EmployeeID string
		Department string
		Position   string
	}{
		{
			{

				EmployeeID: "asd",
				Department: "asd",
				Position:   "asd",
			},
		},
		{
			{

				EmployeeID: "180001",
				Department: "zxc",
				Position:   "asd",
			},
		},
	}

	var req []*http.Request

	for i, _ := range data2 {
		body, _ := json.Marshal(data2[i])
		req = append(req, httptest.NewRequest("PUT", "/updatedepartmentingroup", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Employee wrong",
			testW,
			args{w, req[0]},
		},
		{
			"Department wrong",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.ChangeDepartmentEmployee(tt.args.w, tt.args.r)
		})
	}
}
