package main

import (
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/negroni"
)

func AuthenHandler(r route) *negroni.Negroni {
	var handlers []negroni.Handler

	switch r.AuthenMethod {
	case 1:
		handlers = append(handlers, apiUserEmployee())
	case 2:
		handlers = append(handlers, apiSawmill())
	case 3:
		handlers = append(handlers, apiHR())
	}

	log.Println("Handler: ", handlers)

	handlers = append(handlers, negroni.Wrap(r.HandlerFunc))

	return negroni.New(handlers...)
}

func apiUserEmployee() negroni.HandlerFunc {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

		id, role, err := CheckToken(w, r)
		if err != nil {
			log.Errorln("Token Wrong")
			JSON(w, 400, "Token Wrong")
			return
		}
		lower := strings.ToLower(role)
		if strings.Compare(lower, "admin") != 0 && strings.Compare(lower, "manager") != 0 {
			log.Errorln("Admin or Manager can on access : ", lower, " at id : ", id)
			JSON(w, 401, "Admin or Manager can on access :"+lower+" at id : "+id)
			return
		}

		next(w, r)
	})
}

func apiSawmill() negroni.HandlerFunc {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

		id, role, err := CheckToken(w, r)
		if err != nil {
			log.Errorln("Token Wrong")
			JSON(w, 400, "Token Wrong")
			return
		}
		lower := strings.ToLower(role)
		if strings.Compare(lower, "admin") != 0 && strings.Compare(lower, "qc") != 0 && strings.Compare(lower, "manager") != 0 {
			log.Errorln("QC or admin or Manager can on access : ", lower, " at id : ", id)
			JSON(w, 401, "QC or admin or Manager can on access :"+lower+" at id : "+id)
			return
		}
		next(w, r)
	})
}

func apiHR() negroni.HandlerFunc {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

		id, role, err := CheckToken(w, r)
		if err != nil {
			log.Errorln("Token Wrong")
			JSON(w, 400, "Token Wrong")
			return
		}
		lower := strings.ToLower(role)
		if strings.Compare(lower, "admin") != 0 && strings.Compare(lower, "hr") != 0 && strings.Compare(lower, "manager") != 0 {
			log.Errorln("HR or admin or Manager can on access : ", lower, " at id : ", id)
			JSON(w, 401, "HR or admin or Manager can on access :"+lower+" at id : "+id)
			return
		}
		next(w, r)
	})
}
