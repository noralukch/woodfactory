package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_wood_InsertTable(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := [][]struct {
		Employeeid string
		Position   string
		Rate       float64
	}{
		{
			{
				"a",
				"นายม้า",
				0.1,
			},
			{
				"a",
				"นายม้า",
				0.1,
			},
		},
		{
			{
				"a",
				"นายม้า",
				0.1,
			},
			{
				"b",
				"นายม้า",
				0.1,
			},
		},
		{
			{
				"a",
				"หางม้า",
				0.1,
			},
			{
				"b",
				"หางม้า",
				0.1,
			},
		},
		{
			{
				"b",
				"นายม้า",
				10,
			},
			{
				"a",
				"หางม้า",
				0.1,
			},
		},
		{
			{
				"a",
				"นายม้า",
				0.9,
			},
			{
				"b",
				"หางม้า",
				0.1,
			},
		},
		{
			{
				"180015",
				"นายม้า",
				0.1,
			},
			{
				"180016",
				"หางม้า",
				0.9,
			},
		},
		{
			{
				"180011",
				"หางม้า",
				0.1,
			},
			{
				"180012",
				"นายม้า",
				0.9,
			},
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/inserttable", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Employee repeat",
			testW,
			args{w, req[0]},
		},
		{
			"นายม้า repeat",
			testW,
			args{w, req[1]},
		},
		{
			"หางม้า repeat",
			testW,
			args{w, req[2]},
		},
		{
			"Rate not correct",
			testW,
			args{w, req[3]},
		},
		{
			"Employee don't have",
			testW,
			args{w, req[4]},
		},
		{
			"Employee don't get in รายวันโต๊ะเลื่อย",
			testW,
			args{w, req[5]},
		},
		{
			"Employee get in other table",
			testW,
			args{w, req[6]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.InsertTable(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_UpdateEmployeeInTable(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	type tmpEmtable struct {
		Employeeid string
		Position   string
		Rate       float64
	}

	data := []struct {
		Date    string
		Notable string
		Emtable []tmpEmtable
	}{
		{
			"asd",
			"asd",
			[]tmpEmtable{
				{
					Employeeid: "a",
					Position:   "นายม้า",
					Rate:       0.9,
				},
				{
					Employeeid: "a",
					Position:   "นายม้า",
					Rate:       0.1,
				},
			},
		},
		{
			"asd",
			"asd",
			[]tmpEmtable{
				{
					Employeeid: "a",
					Position:   "นายม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "b",
					Position:   "นายม้า",
					Rate:       0.1,
				},
			},
		},
		{
			"asd",
			"asd",
			[]tmpEmtable{
				{
					Employeeid: "a",
					Position:   "หางม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "b",
					Position:   "หางม้า",
					Rate:       0.1,
				},
			},
		},
		{
			"asd",
			"asd",
			[]tmpEmtable{
				{
					Employeeid: "z",
					Position:   "นายม้า",
					Rate:       10,
				},
				{
					Employeeid: "a",
					Position:   "หางม้า",
					Rate:       0.1,
				},
			},
		},
		{
			"04September2018",
			"1",
			[]tmpEmtable{
				{
					Employeeid: "a",
					Position:   "นายม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "b",
					Position:   "หางม้า",
					Rate:       0.9,
				},
			},
		},
		{
			"04September2018",
			"1",
			[]tmpEmtable{
				{
					Employeeid: "180015",
					Position:   "นายม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "180016",
					Position:   "หางม้า",
					Rate:       0.9,
				},
			},
		},
		{
			"04September2018",
			"1",
			[]tmpEmtable{
				{
					Employeeid: "180011",
					Position:   "หางม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "180012",
					Position:   "นายม้า",
					Rate:       0.9,
				},
			},
		},
		{
			"08September2018",
			"1",
			[]tmpEmtable{
				{
					Employeeid: "180011",
					Position:   "หางม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "180012",
					Position:   "นายม้า",
					Rate:       0.9,
				},
			},
		},
		{
			"04September2018",
			"12",
			[]tmpEmtable{
				{
					Employeeid: "180015",
					Position:   "นายม้า",
					Rate:       0.1,
				},
				{
					Employeeid: "180016",
					Position:   "หางม้า",
					Rate:       0.9,
				},
			},
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("PUT", "/updateemployeeintable", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Employee repeat",
			testW,
			args{w, req[0]},
		},
		{
			"นายม้า repeat",
			testW,
			args{w, req[1]},
		},
		{
			"หางม้า repeat",
			testW,
			args{w, req[2]},
		},
		{
			"Rate not correct",
			testW,
			args{w, req[3]},
		},
		{
			"Employee don't have",
			testW,
			args{w, req[4]},
		},
		{
			"Employee don't get in รายวันโต๊ะเลื่อย",
			testW,
			args{w, req[5]},
		},
		{
			"Employee get in other table",
			testW,
			args{w, req[6]},
		},
		{
			"Dont have Date",
			testW,
			args{w, req[7]},
		},
		{
			"Don't have Notable",
			testW,
			args{w, req[8]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.UpdateEmployeeInTable(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_UpdateDetailWood(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	type tmp []struct {
		Size          string
		AmountWood    float64
		AmountBadwood float64
		Cost          float64
	}

	data := []struct {
		Date    string
		Notable string
		Grade   string
		Detail  []struct {
			Size          string
			AmountWood    float64
			AmountBadwood float64
			Cost          float64
		}
	}{
		{
			"asd",
			"a",
			"a",
			tmp{
				{
					"asd",
					12,
					11,
					10,
				},
			},
		},
		{
			"04September2018",
			"a",
			"ไม้ระแนง",
			tmp{
				{
					"0.5\"x 0.5\"x 1.30 ม.",
					12,
					11,
					10,
				},
			},
		},
		{
			"04September2018",
			"1",
			"a",
			tmp{
				{
					"0.5\"x 0.5\"x 1.30 ม.",
					12,
					11,
					10,
				},
			},
		},
		{
			"04September2018",
			"1",
			"ไม้ระแนง",
			tmp{
				{
					"0.5\"x 0.7\"x 1.0 ม.",
					12,
					11,
					10,
				},
			},
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("PUT", "/updatewooddetail", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Don't have date",
			testW,
			args{w, req[0]},
		},
		{
			"Wrong notable",
			testW,
			args{w, req[1]},
		},
		{
			"Wrong grade",
			testW,
			args{w, req[2]},
		},
		{
			"Wrong size",
			testW,
			args{w, req[3]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.UpdateDetailWood(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_UpdateWeightWood(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Date       string
		Notable    string
		WeightWood float64
		WeightWing float64
		Remark     string
	}{
		{
			"asd",
			"asd",
			0,
			0,
			"",
		},
		{
			"asd",
			"asd",
			-2,
			-2,
			"",
		},
		{
			"asd",
			"asd",
			1,
			1,
			"",
		},
		{
			"04September2018",
			"asd",
			1,
			1,
			"",
		},
		{
			"04September2018",
			"1",
			1,
			1,
			"",
		},
	}

	var req []*http.Request

	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("PUT", "/updateweightwood", bytes.NewReader(body)))
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Weightwood = 0",
			testW,
			args{w, req[0]},
		},
		{
			"Weightwood < 0",
			testW,
			args{w, req[1]},
		},
		{
			"Don't have date",
			testW,
			args{w, req[2]},
		},
		{
			"Don't have notable",
			testW,
			args{w, req[3]},
		},
		{
			"Success",
			testW,
			args{w, req[4]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.UpdateWeightWood(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_DeleteTable(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Date string
		No   string
	}{
		{
			"asd",
			"asd",
		},
		{
			"04September2018",
			"asd",
		},
	}

	var req []*http.Request

	for _, value := range data {
		r := httptest.NewRequest("DELETE", "/deletetable", nil)
		r.Header.Set("Day", value.Date)
		r.Header.Add("NoTable", value.No)
		req = append(req, r)
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong date",
			testW,
			args{w, req[0]},
		},
		{
			"Wrong table",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.DeleteTable(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_DeleteDateSawmill(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []string{""}

	var req []*http.Request

	for _, value := range data {
		r := httptest.NewRequest("DELETE", "/deletedatesawmill", nil)
		r.Header.Add("NoTable", value)
		req = append(req, r)
	}

	w := httptest.NewRecorder()

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong Date",
			testW,
			args{w, req[0]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.DeleteDateSawmill(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetEmployeeinTable(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Date string
	}{
		{
			"asd",
		},
		{
			"04September2018",
		},
	}

	var req []*http.Request
	w := httptest.NewRecorder()
	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/getemployeeintable", bytes.NewReader(body)))
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong Date",
			testW,
			args{w, req[0]},
		},
		{
			"Success",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetEmployeeinTable(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetSawmill(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/getsawmill", nil)

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Success",
			testW,
			args{w, req},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetSawmill(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetDateSawmill(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Date string
	}{
		{
			"asd",
		},
		{
			"04September2018",
		},
	}

	var req []*http.Request
	w := httptest.NewRecorder()
	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/getdatesawmill", bytes.NewReader(body)))
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong Date",
			testW,
			args{w, req[0]},
		},
		{
			"Success",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetDateSawmill(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetDateTotal(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		Date string
	}{
		{
			"asd",
		},
		{
			"04September2018",
		},
	}

	var req []*http.Request
	w := httptest.NewRecorder()
	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/getdatetotalsawmill", bytes.NewReader(body)))
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong Date",
			testW,
			args{w, req[0]},
		},
		{
			"Success",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetDateTotal(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetNameEmployee(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	data := []struct {
		EmployeeID string
	}{
		{
			"asd",
		},
		{
			"180001",
		},
	}

	var req []*http.Request
	w := httptest.NewRecorder()
	for i, _ := range data {
		body, _ := json.Marshal(data[i])
		req = append(req, httptest.NewRequest("POST", "/getnameemployee", bytes.NewReader(body)))
	}

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Wrong Employee",
			testW,
			args{w, req[0]},
		},
		{
			"Success",
			testW,
			args{w, req[1]},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetNameEmployee(tt.args.w, tt.args.r)
		})
	}
}

func Test_wood_GetTodaySawmill(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/gettodaysawmill", nil)

	tests := []struct {
		name string
		wd   *wood
		args args
	}{
		// TODO: Add test cases.
		{
			"Success",
			testW,
			args{w, req},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wd.GetTodaySawmill(tt.args.w, tt.args.r)
		})
	}
}
