package main

import (
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

func (wd *wood) InsertWood(w http.ResponseWriter, r *http.Request) {
	var wood struct {
		//Type       string  `json:"type"`
		WeightWood float64 `json:"weightwood"`
		//CostWood   float64 `json:"costwood"`
	}

	if err := json.NewDecoder(r.Body).Decode(&wood); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, http.StatusInternalServerError, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	tmpWoodin := WoodStorage{
		Date: time.Now().Format("02-01-2006"),
		//	Type:       wood.Type,
		WeightWood: wood.WeightWood,
		//	CostWood:   wood.CostWood,
		//Total: wood.WeightWood * wood.CostWood,
	}

	err := InsertWoodDB(tmpWoodin)
	if err != nil {
		log.Errorln("Cannot Insert Wood ") //, wood.Type)
		JSON(w, 404, "Cannot Insert Wood")
		return
	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, http.StatusOK, res)
}

func (wd *wood) GetWood(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()

	wood, err := FindWood()
	if err != nil {
		log.Errorln("Cannot Get All Wood")
		JSON(w, 500, "Cannot Get All Wood")
		return
	}

	res := struct {
		Wood []WoodStorage `json:"wood"`
	}{wood}

	JSON(w, http.StatusOK, res)
}
