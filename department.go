package main

import (
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

func (wd *wood) InsertDepartment(w http.ResponseWriter, r *http.Request) {
	var dm struct {
		Name string `json:"name" `
	}

	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&dm); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	if len(dm.Name) == 0 {
		log.Errorln("Please enter name of department")
		JSON(w, 400, "Please enter name of department")
		return
	}

	_, err := FindOneDepartment(dm.Name)
	if err == nil {
		log.Errorln("This Department was use")
		JSON(w, 400, "This Department was use")
		return
	}

	tmpDepartment := Department{
		Name: dm.Name,
	}

	err = InsertDepartment(tmpDepartment)
	if err != nil {
		log.Errorln("Can't Insert Department", err)
		JSON(w, 500, "Can't Insert Department")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) ChangeDepartmentEmployee(w http.ResponseWriter, r *http.Request) {
	var data []struct {
		EmployeeID string `json:"employeeid"`
		Department string `json:"department"`
		Position   string `json:"position"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	for _, loop := range data {
		em, err := FindEmployee(loop.EmployeeID)
		if err != nil {
			log.Errorln("Can't fine this employee : ", loop.EmployeeID)
			JSON(w, 204, "Can't fine this employee : "+loop.EmployeeID)
			return
		}

		if em.Department == "รายวันโต๊ะเลื่อย" && em.Department != loop.Department {
			date := time.Now().Format("02/01/2006")

			sm, err := FindDateSawmill(date)
			if err != nil {
				log.Errorln("Can't find Sawmill today ", err)
				JSON(w, 204, "Can't find Sawmill today")
				return
			}

			var tmp_em EmTable

			for i, no := range sm.NoTable {
				for j, em_table := range no.EmTable {
					if em_table.Employeeid == em.EmployeeID {
						sm.NoTable[i].EmTable[j] = tmp_em
					}
				}
			}
			err = UpdateSawmill(sm)
			if err != nil {
				log.Errorln("Can't Update Table : ", sm.Date)
				JSON(w, 500, "Can't Update Table :"+sm.Date)
				return
			}
		}

		old_dm, err := FindOneDepartment(em.Department)
		if err != nil {
			log.Errorln("Can't fine this department : ", em.Department)
			JSON(w, 204, "Can't fine this department : "+em.Department)
			return
		}

		len := len(old_dm.Employee)

		for i, num_em := range old_dm.Employee {
			if em.EmployeeID == num_em {
				if i == 0 {
					old_dm.Employee = old_dm.Employee[i+1:]
				} else if i == len-1 {
					old_dm.Employee = old_dm.Employee[:i]
				} else {
					tmpEM := old_dm.Employee[:i]
					tmpEM = append(tmpEM, old_dm.Employee[i+1:]...)

					old_dm.Employee = tmpEM
				}

				break
			}
		}

		if em.Position == "หัวหน้าแผนก" {
			old_dm.ManagerID = ""
			old_dm.Manager = ""
		}

		new_dm, err := FindOneDepartment(loop.Department)
		if err != nil {
			log.Errorln("Can't fine this department : ", loop.Department)
			JSON(w, 204, "Can't fine this department : "+loop.Department)
			return
		}

		check := 0

		for _, de_em := range new_dm.Employee {
			if de_em == loop.EmployeeID {
				check = 1
			}
		}
		if check == 0 {
			new_dm.Employee = append(new_dm.Employee, loop.EmployeeID)
		}

		if loop.Position == "หัวหน้าแผนก" {
			if new_dm.ManagerID != "" {
				// num := strings.Index(new_dm.Manager, " ")
				// first := new_dm.Manager[:num]
				// second := new_dm.Manager[num+1:]
				old_manager, err := FindEmployee(new_dm.ManagerID)
				if err != nil {
					log.Errorln("Can't fine this employee : ", new_dm.ManagerID)
					JSON(w, 204, "Can't fine this employee : "+new_dm.ManagerID)
					return
				}
				old_manager.Position = ""
				err = UpdateEmployee(old_manager)
				if err != nil {
					log.Errorln("Can't update employee : ", err)
					JSON(w, 500, "Can't update employee ")
					return
				}
			}
			new_dm.ManagerID = em.EmployeeID
			new_dm.Manager = em.NameTitle + " " + em.Name + " " + em.Surname
		}

		em.Department = loop.Department
		em.Position = loop.Position

		err = UpdateDepartment(old_dm)
		if err != nil {
			log.Errorln("Can't Update old department", err)
			JSON(w, 500, "Can't Update old department")
			return
		}

		err = UpdateDepartment(new_dm)
		if err != nil {
			log.Errorln("Can't Update new department ", err)
			JSON(w, 500, "Can't Update new department")
			return
		}

		err = UpdateEmployee(em)
		if err != nil {
			log.Errorln("Can't Update employee : ", err)
			JSON(w, 500, "Can't Update employee")
			return
		}

	}

	res := struct {
		Success bool `json:"success"`
	}{true}

	JSON(w, 200, res)
	return
}

func (wd *wood) UpdateDepartment(w http.ResponseWriter, r *http.Request) {
	var dm struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}
	if err := json.NewDecoder(r.Body).Decode(&dm); err != nil {
		log.Errorln("JSON Decode FAIL:", err)
		JSON(w, 400, "JSON Decode FAIL")
		return
	}

	defer r.Body.Close()

	if len(dm.ID) == 0 || len(dm.Name) == 0 {
		log.Errorln("Please enter name of department")
		JSON(w, 400, "Please enter name of department")
		return
	}

	id := bson.ObjectIdHex(dm.ID)

	new_dm, err := FindOneDepartmentbyID(id)
	if err != nil {
		log.Error("Can't find department")
		JSON(w, 204, "Can't find department")
		return
	} else {
		_, err := FindOneDepartment(dm.Name)
		if err == nil && new_dm.Name != dm.Name {
			log.Error("Name was use")
			JSON(w, 400, "Name was use")
			return
		}
	}

	for _, em := range new_dm.Employee {
		em_dm, err := FindEmployee(em)
		if err != nil {
			log.Error("Can't find employee : ", em)
			JSON(w, 204, "Can't find employee : "+em)
			return
		}

		em_dm.Department = dm.Name

		err = UpdateEmployee(em_dm)
		if err != nil {
			log.Error("Can't update employee : ", err)
			JSON(w, 500, "Can't update employee")
			return
		}
	}

	new_dm.Name = dm.Name

	err = UpdateDepartmentbyID(new_dm)
	if err != nil {
		log.Error("Can't update Department : ", err)
		JSON(w, 500, "Can't update Department")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) DeleteDepartment(w http.ResponseWriter, r *http.Request) {
	ID := r.Header.Get("ID")

	id := bson.ObjectIdHex(ID)
	defer r.Body.Close()

	dm, err := FindOneDepartmentbyID(id)
	if err != nil {
		log.Errorln("This department id don't have : ", id)
		JSON(w, 204, "This department id don't have : "+id)
		return
	}

	_, err = FindDepartmentEmployee(dm.Name)
	if err == nil {
		log.Errorln("This department have employees")
		JSON(w, 400, "This department have employees")
		return
	}

	err = DelDepartment(dm.Name)
	if err != nil {
		log.Errorln("Delete Department error: ", err)
		JSON(w, 500, "Delete Department error ")
		return
	}

	res := struct {
		Success bool `json:"success" `
	}{true}

	JSON(w, http.StatusOK, res)
	return
}

func (wd *wood) GetDepartment(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()

	dm, err := FindDepartment()
	if err != nil {
		log.Errorln("Can't Get All Department ", err)
		JSON(w, 500, "Can't Get All Department ")
		return
	}

	var department []struct {
		ID        bson.ObjectId `json:"id"`
		Name      string        `json:"name"`
		ManagerID string        `json:"managerid"`
		Manager   string        `json:"manager"`
		Tel       string        `json:"tel"`
	}

	for _, loop_dm := range dm {

		if loop_dm.ManagerID != "" {

			em, err := FindEmployee(loop_dm.ManagerID)
			if err != nil {
				log.Errorln("Can't find employee : ", loop_dm.ManagerID)
				JSON(w, 500, "Can't find employee : "+loop_dm.ManagerID)
				return
			}

			res := struct {
				ID        bson.ObjectId `json:"id"`
				Name      string        `json:"name"`
				ManagerID string        `json:"managerid"`
				Manager   string        `json:"manager"`
				Tel       string        `json:"tel"`
			}{loop_dm.ID, loop_dm.Name, em.EmployeeID, em.NameTitle + " " + em.Name + " " + em.Surname, em.Tel}

			department = append(department, res)
		} else {
			res := struct {
				ID        bson.ObjectId `json:"id"`
				Name      string        `json:"name"`
				ManagerID string        `json:"managerid"`
				Manager   string        `json:"manager"`
				Tel       string        `json:"tel"`
			}{loop_dm.ID, loop_dm.Name, loop_dm.ManagerID, loop_dm.Manager, ""}

			department = append(department, res)
		}

	}

	JSON(w, http.StatusOK, department)
	return
}

func (wd *wood) GetDepartmentDetail(w http.ResponseWriter, r *http.Request) {
	Name := r.Header.Get("Department")

	id := bson.ObjectIdHex(Name)
	defer r.Body.Close()

	one_dm, err := FindOneDepartmentbyID(id)
	if err != nil {
		log.Errorln("This department id don't have : ", id)
		JSON(w, 204, "This department id don't have : "+id)
		return
	}

	var detail []struct {
		EmployeeID   string `json:"employeeid"`
		EmployeeName string `json:"employeename"`
		Position     string `json:"position"`
	}

	for _, loop_dm := range one_dm.Employee {
		em, err := FindEmployee(loop_dm)
		if err != nil {
			log.Errorln("Can't find employee : ", em.EmployeeID)
			JSON(w, 204, "Can't find employee : "+em.EmployeeID)
			return
		}

		res := struct {
			EmployeeID   string `json:"employeeid"`
			EmployeeName string `json:"employeename"`
			Position     string `json:"position"`
		}{em.EmployeeID, em.NameTitle + " " + em.Name + " " + em.Surname, em.Position}

		detail = append(detail, res)
	}

	if len(one_dm.Employee) == 0 {
		res := struct {
			EmployeeID   string `json:"employeeid"`
			EmployeeName string `json:"employeename"`
			Position     string `json:"position"`
		}{"", "", ""}

		detail = append(detail, res)
	}
	JSON(w, http.StatusOK, detail)
	return
}
